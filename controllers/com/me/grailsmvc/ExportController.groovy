package com.me.grailsmvc

import grails.transaction.Transactional

@Transactional(readOnly = true)
class ExportController {

    //ExportService exportService

    static allowedMethods = [save: "POST"]

    def exportForm() {
        def documents = Documents.findAll() // For before save...
        [documents: documents, export: new Export()]
    }

    @Transactional
    def save() {
        int i = 0  // for while loop, hare because of the following line...
        int j = 0
        def epExportMaterial = new EpExportMaterial(params["EpExportMaterial[" + i + "]"])
        def export = new Export(params)
        def documents = Documents.findAll() // for after save...

        // To validate form fields...
        if (export.validate() & epExportMaterial.validate()) {

            // For master detail mapping...
            while (params["EpExportMaterial[" + i + "]"] != null) {
                export.addToEpExportMaterial(new EpExportMaterial(params["EpExportMaterial[" + i + "]"]))
                i++
            }

            // Finding base path of project directory...
            def applicationContext = grailsApplication.mainContext
            String basePath = applicationContext.getResource("/").getFile().toString()

            while (params["Attachments[" + j + "]"] != null) {
                if (params["Attachments[" + j + "].files"].fileItem.fileName != ""
                        && params["Attachments[" + j + "].files"].fileItem.fileName != null) {
                    def mlcFile = params["Attachments[" + j + "].files"]
                    def mlcFileName = mlcFile.fileItem.fileName
                    def docIdVal = params["Attachments[" + j + "].docId"]

                    def attachments = new Attachments()

                    attachments.setFiles(mlcFileName)
                    attachments.setDocId(docIdVal)
                    // Adding master object with detail...
                    export.addToAttachments(attachments)
                    // Existing directory with file...
                    def mlcFilePath = new File("${basePath}/../web-app/myFolder/exportfiles", mlcFileName)
                    //File saving to the directory...
                    if (!mlcFilePath.exists()) {
                        mlcFile.transferTo(mlcFilePath)
                    }
                }

                j++
            }

            export.save(flush: true, failOnError: true)

            //Page redirecting method...
            redirect(action: "exportForm", false: true)
            flash.message = "Data Saved Successfully !"
        } else {
            render(view: "exportForm", model: [export: export, epExportMaterial: epExportMaterial, documents: documents])
            flash.error = "Something went wrong ! Please see inside the form..."
        }

        // [export: export]
    }

    def viewExport() {
        def export = Export.get(params.id)
        [export: export]
    }

    def updateExport() {
        def export = Export.get(params.id)
        render(view: "exportForm", model: [export: export])
    }

    def deleteExport = {
        def contact = Export.get(params.id)
        contact.delete flush: true, failOnError: true
        redirect(url: "/export/listExport?offset=0&max=5")
    }

    def listExport() {
        [exports: Export.list(params), exportCount: Export.count()]
    }


}
