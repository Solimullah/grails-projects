package com.me.grailsmvc


import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class CountryController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def countryForm(){
        return "countryForm"
    }

    def listCountry() {
        [countries: Country.list(params), cocountryInstanceCount: Country.count()]
    }

    def show(Country countryInstance) {
        respond countryInstance
    }

    def create() {
        respond new Country(params)
    }

    @Transactional
    def save() {
        def  country = new Country(params)
        country.save flush: true
        redirect(action: "countryForm", flash: true)
        flash.message = "The Country Saved Successfully !"
    }

    def edit(Country countryInstance) {
        respond countryInstance
    }

    @Transactional
    def update(Country countryInstance) {
        if (countryInstance == null) {
            //notFound()
            return
        }

        if (countryInstance.hasErrors()) {
            respond countryInstance.errors, view: 'edit'
            return
        }

        countryInstance.save flush: true
    }

    @Transactional
    def deleteCountry() {
        def countryInstance = Country.get(params.id)
        countryInstance.delete flush: true
        redirect(url: "/country/listCountry?offset=0&max=5#", flash: true)
    }


}
