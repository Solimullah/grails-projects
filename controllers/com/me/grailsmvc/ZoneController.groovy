package com.me.grailsmvc

import grails.converters.JSON
import grails.transaction.Transactional

class ZoneController {

    static allowedMethods = [saveEnterprise: "POST"]

    def zoneForm() {
        return "zoneForm"
    }


    def saveEnterprise() {
//        def zone_i = session["zoneId"]

        def zone_id = params.long("zoneId")  // finding zoneId by dropdown name from zoneForm.gsp...
        def zone = Zone.get(zone_id) // getting Zone object(data) according to its zone_id...
        def enterprise = new Enterprise(params)

        enterprise.zone = zone // initializing zone's fields of enterprise by its details / Relation making...

        if (enterprise.validate()) {
            enterprise.save(flush: true)
            redirect(action: "zoneForm", false: true)
            flash.message = "Enterprise Saved Successfully."
        } else {
            redirect(action: "zoneForm", false: true)
            flash.error = "An Error has occurred !!.."
        }

    }



    def getEnterpriseByZoneId() {
        def zoneId = params.long("zoneId")
        def enterpriseList
        def sb = new StringBuilder()
//        enterpriseList1 = getEnterpriseByZoneId(zoneId)
        enterpriseList = Enterprise.findAll("from Enterprise where zone_id =" + zoneId)
        sb.append('<option value="">Select One</option>')


        enterpriseList.each {
            sb.append('<option value="' + it.id + '">' + it.enterpriseName + '</option>')
        }
        render sb
    }


}
