package com.me.grailsmvc

class Attachments {

    Integer id
    String docId
    String files
    Boolean isActive


    static constraints = {
        docId nullable: true
        files nullable: false
        isActive nullable: true
    }

    static mapping = {
        table("attachments")
        version(false)
    }
}
