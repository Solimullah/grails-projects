package com.me.grailsmvc

import org.grails.databinding.BindingFormat


class Export {
    Integer id;
    String zoneName = "Cumilla Export Processing Zone";
    String entprzName = "Nassa Spinning Ltd.";
    String exPermitNo
    @BindingFormat("dd/MM/yyyy") // To parse date format...
    Date exPermitDate;
    //@NotEmpty(message = "Invoice No. Required.")
    String invoiceNo;
    @BindingFormat("dd/MM/yyyy") // To parse date format...
    Date invoiceDate;
    String lcNo;
    @BindingFormat("dd/MM/yyyy") // To parse date format...
    Date lcDate;//
    String ipItemName;
    int ipItemQty;
    String ipUnitOfQty;
    String importCountry;
    String ipNo;
    @BindingFormat("dd/MM/yyyy") // To parse date format...
    Date ipDate;
    String billOfEntryNo;
    @BindingFormat("dd/MM/yyyy") // To parse date format...
    Date billOfEntryDate;


    static hasMany = [epExportMaterial: EpExportMaterial, attachments: Attachments]

    static constraints = {
        exPermitNo blank: false
        exPermitDate nullable: false
        invoiceNo nullable: false
        invoiceDate nullable: false
        lcNo nullable: true
        lcDate nullable: true//
        ipItemName nullable: false
        ipItemQty nullable: false
        ipUnitOfQty nullable: false
        importCountry nullable: false
        ipNo nullable: false
        ipDate nullable: false
        billOfEntryNo nullable: false
        billOfEntryDate nullable: false

    }

    static mapping = {
        table("export")
        version(false)
    }


}
