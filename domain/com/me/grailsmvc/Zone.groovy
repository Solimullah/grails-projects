package com.me.grailsmvc

class Zone {
    String zoneName
    String address
    static hasMany = [enterprises: Enterprise]

    static constraints = {
        zoneName nullable: false
        address nullable: true
    }

    static mapping = {
        table("zone")
        version(false)
    }
}
