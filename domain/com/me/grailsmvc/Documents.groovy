package com.me.grailsmvc

class Documents {

    Integer id
    String docName

    static constraints = {
        docName nullable: true
    }

    static mapping = {
        table("documents")
        version(false)
    }
}
