package com.me.grailsmvc

class Enterprise {
    String enterpriseName
    static belongsTo = [zone: Zone]

    static constraints = {
        enterpriseName nullable: false
    }

    static mapping = {
        table("enterprise")
        //id generator: 'assigned'
        version(false)
    }
}
