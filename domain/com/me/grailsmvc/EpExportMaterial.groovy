package com.me.grailsmvc


class EpExportMaterial {

    Integer id
    String exItemName
    int exItemQty
    String exUnitOfQty
    String consigneeName
    String consigneeAddress

    //static belongsTo = [export: Export]  //To map with foreign key...

    static constraints = {
        exItemName nullable: false
        exItemQty nullable: false
        exUnitOfQty nullable: false
        consigneeName nullable: true
        consigneeAddress nullable: true
    }

    static mapping = {
        table("ep_export_material")
        version(false)
    }

}
