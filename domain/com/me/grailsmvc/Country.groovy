package com.me.grailsmvc

class Country {

    Integer id
    String countryName

    static constraints = {
        countryName nullable: false
    }

    static mapping = {
        table("country")
        version(false)
    }

}
