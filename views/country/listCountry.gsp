<%--
  Created by IntelliJ IDEA.
  User: Solimullah&Amena
  Date: 12-Oct-19
  Time: 12:19 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script>
    <script async="" src="https://www.google-analytics.com/analytics.js"></script>
    <script async="" src="https://www.googletagmanager.com/gtm.js?id=GTM-MFSJQFR"></script>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    <!--style sheet link-->
    <asset:link rel="stylesheet" type="text/css" href="/folder/mycssresource/mycss.css"/>
    <asset:link rel="stylesheet" type="text/css" href="/folder/mycssresource/export/css/AdminLTE.min.css"/>
    <asset:link rel="stylesheet" type="text/css" href="/folder/mycssresource/export/css/bootstrap-datepicker.min.css"/>
    <asset:link rel="stylesheet" type="text/css" href="/folder/mycssresource/export/css/common.css"/>
    <asset:link rel="stylesheet" type="text/css" href="/folder/mycssresource/export/css/style.css"/>
    <asset:link rel="stylesheet" type="text/css" href="/folder/mycssresource/export/css/dataTables.bootstrap.css"/>

    <!--Date picker link-->
    <!-- Vendor CSS-->
    <%--<link rel="stylesheet" type="text/css" href="<c:url value="/folder/userResources/vendor/datepicker/daterangepicker.css"/>" media="all">--%>

    <!--for <i class="fa fa-reorder"></i> teg link-->
    <%--
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    --%>
    <!-- Or this Icons font CSS-->
    <asset:link rel="stylesheet" type="text/css"
                href="/folder/userResources/vendor/font-awesome-4.7/css/font-awesome.min.css" media="all"/>

    <script>
        function deleteProduct(countryId) {
            if (confirm('Do you really want to delete this country?')) {
                var url = '/Export/country/deleteCountry/'+ countryId;
                window.location.href = url;
            }
        }
    </script>
</head>

<body class="skin-blue sidebar-mini sidebar-collapse pace-done">
<div class="pace  pace-inactive">
    <div class="pace-progress" data-progress-text="100%" data-progress="99"
         style="transform: translate3d(100%, 0px, 0px);">
        <div class="pace-progress-inner"></div>
    </div>

    <div class="pace-activity"></div>
</div>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper custom_mergin" style="min-height: 813px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h1><a href="/Export/">
            <img src="/Export/assets/bepzaLogo.png" height="50px" width="60px">
            </a>
            Country List</h1>
        <ul class="top-links">
            <li>
                <a href="/Export/country/countryForm" class="btn btn-block btn-primary btn-xs">
                    <i class="fa fa-plus"></i> Add New Country
                </a>
            </li>
        </ul>
    </section><!-- /.content-header -->

    <g:if test="${flash.message}">
        <section class="flash-message">
            <!-- flash message -->
            <div class="bs-example">
                <div class="alert alert-success small fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong style="font-size: 15px">${flash.message}</strong>
                </div>
            </div>
            <!-- flash message end -->
        </section><!-- /.box-header (Flash Message) -->
    </g:if>

<!-- For Error message-->
    <g:if test="${flash.error}">
        <section class="flash-message">
            <!-- flash message -->
            <div class="bs-example">
                <div class="alert alert-error small fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong style="font-size: 15px">${flash.error}</strong>
                </div>
            </div>
            <!-- flash message end -->
        </section><!-- /.box-header (Flash Message) -->
    </g:if>

    <section class="content"><!-- Main content -->
    <!-- Serch box -->
        <div class="box box-info">
            <form action="/bepza/dcMstDyeingCertificate/list" method="get">
                <fieldset>
                    <div class="box-header with-border">
                        <legend><h3 class="box-title">Search</h3></legend>

                        <div class="box-body">
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="form-group">
                                    <label>DC Permit No./Tracking No.</label>
                                    <input type="text" name="dcPermitNo" id="dcPermitNo" value="" class="form-control">
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="form-group">
                                    <label>Zone</label>
                                    <select id="dcZone" name="dcZoneId" class="form-control">
                                        <option value="">Select One</option>
                                        <option value="7">Cumilla Export Processing Zone</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="form-group">
                                    <label>Enterprise</label>
                                    <select id="enterprise" name="enterpriseId" class="form-control">
                                        <option value="">Select One</option>
                                        <option value="1768">Enzychim Iberica (BD) Limited</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="form-group">
                                    <label>Current Status</label>
                                    <select id="currentStatus" name="currentStatus" class="form-control">
                                        <option value="">Select One</option>
                                        <option value="37226">Checked and verified for meeting</option>
                                        <option value="37227">Update meeting decision</option>
                                        <option value="37228">Verify and update</option>
                                        <option value="37229">Placed in meeting</option>
                                        <option value="37243">Scrutiny</option>
                                        <option value="47790">Deny for cancel</option>
                                        <option value="2831">Draft</option>
                                        <option value="2832">Re Submitted</option>
                                        <option value="3000">Issued</option>
                                        <option value="2715">Prepared</option>
                                        <option value="27160">Submitted</option>
                                        <option value="2716">Checked &amp; Submitted</option>
                                        <option value="2717">Checked and Verified</option>
                                        <option value="2718">Forwarded</option>
                                        <option value="2721">Delegate</option>
                                        <option value="2719">Permitted by BEPZA</option>
                                        <option value="3099">Regret</option>
                                        <option value="2722">Import Delegate</option>
                                        <option value="2723">SCP Delegate</option>
                                        <option value="2724">Gate Pass Issued</option>
                                        <option value="2725">Gate Pass Rejected</option>
                                        <option value="2726">Application Extend</option>
                                        <option value="2727">Permit for Cancel</option>
                                        <option value="2728">Customs Clearance Issued</option>
                                        <option value="2729">Partial Gate Pass</option>
                                        <option value="2730">Partial Shipment (Close)</option>
                                        <option value="2731">Application Cancel</option>
                                        <option value="2732">Checked and Verified for Cancel</option>
                                        <option value="4320">Applied for Extension</option>
                                        <option value="27201">Revised</option>
                                        <option value="37224">Sent Back</option>
                                        <option value="4321">Applied for Cancel</option>
                                        <option value="5263">Forwarded for Engg. Verification</option>
                                        <option value="37314">Applied for Amendment</option>
                                        <option value="37225">Forwarded to Permit Officer</option>
                                        <option value="37214">Recommendation</option>
                                        <option value="37215">Mark to Process</option>
                                        <option value="37249">Delegate</option>
                                        <option value="37306">Scrutiny Found Ok</option>
                                        <option value="37307">Further Scrutiny</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="form-group">
                                    <label>Export Permit No.</label>
                                    <input type="text" name="exportPermitNo" id="exportPermitNo" value=""
                                           class="form-control">
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="form-group">
                                    <label>Commercial Invoice No.</label>
                                    <input type="text" name="invoiceNo" id="invoiceNo" value="" class="form-control">
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="form-group">
                                    <label>Submitted (From Date)</label>
                                    <input type="text" name="fromDate" id="fromDate" value=""
                                           class="form-control js-datepicker" placeholder="dd/mm/yyyy">
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="form-group">
                                    <label>Submitted (To Date)</label>
                                    <input type="text" name="toDate" id="toDate" value=""
                                           class="form-control js-datepicker" placeholder="dd/mm/yyyy">
                                </div>
                            </div>
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-search"></i>Search
                            </button>
                        </div><!-- /.box-footer-->

                    </div>
                </fieldset>
            </form>
        </div><!-- /.box -->

        <div class="box box-primary">
            <div class="box-body table-responsive no-padding">

            <table class="table table-bordered dt-responsive table-striped table-hover table-condensed display-3">
                <thead style="background-color: lightblue">
                <tr>
                    <th class="center">ID No.</th>
                    <th class="center">Name of Country</th>
                    <th class="center">Action</th>
                </tr>
                </thead>
                <g:if test="${countries}"><!-- means if exports exists (!exports means not exists)-->
                    <tbody>

                    <g:each in="${countries}" var="country">
                        <tr>
                            <td class="center">${country.id}</td>
                            <td class="center">${country.countryName}</td>
                            <td class="center">
                                <div class="list-action">

                                    %{-- It works properly. But I want something different...
                                    <a href="/Export/country/deleteCountry/${country.id}" onclick=""
                                       class="btn btn-primary btn-xs td-none" style="text-decoration: none">
                                        <i class="fa fa-file-text-o"></i>
                                        Delete
                                    </a>--}%

                                    <a href="#" onclick="javascript:deleteProduct(${country.id})"
                                       class="btn btn-primary btn-xs td-none" style="text-decoration: none">
                                        <i class="fa fa-file-text-o"></i>
                                        Delete
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </g:each>
                    </tbody>
                    </table>
                </g:if>
            <!-- For Pagination...-->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="pagination pull-right" id="pagination">
                        <g:paginate controller="country" action="listCountry" total="${cocountryInstanceCount}" max="5"
                                    maxsteps="3"/> <!--here 1. max="" confirms number of records in a page; 2. maxsteps="" confirms steps of pagination number-->
                    </div>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

</body>
</html>