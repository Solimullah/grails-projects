<%--
  Created by IntelliJ IDEA.
  User: Solimullah&Amena
  Date: 12-Oct-19
  Time: 12:05 PM
--%>


<%@ page import="com.me.grailsmvc.Documents; com.me.grailsmvc.Country" contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Create Dyeing Certificate</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    <!--style sheet link-->
    <asset:link rel="stylesheet" type="text/css" href="/folder/mycssresource/mycss.css"/>
    <asset:link rel="stylesheet" type="text/css" href="/folder/mycssresource/export/css/AdminLTE.min.css"/>
    <!--Date picker link-->
    <!-- Vendor CSS-->
    <asset:link href="/folder/userResources/vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all"/>

    <!--for <i class="fa fa-reorder"></i> teg link-->

    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">-->

    <!-- Or this Icons font CSS-->
    <asset:link href="/folder/userResources/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet"
                media="all"/>

    <script>(function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({'gtm.start': new Date().getTime(), event: 'gtm.js'});
        var f = d.getElementsByTagName(s)[0], j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-MFSJQFR');</script>
    <script>(function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
        a = s.createElement(o), m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-91387934-1', 'auto');
    ga('send', 'pageview');</script>

</head>


<body class="skin-blue sidebar-mini sidebar-collapse  pace-done">

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper custom_mergin">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h2><a href="/Export/">
            <img src="/Export/assets/bepzaLogo.png" height="50px" width="60px">
        </a>Country Addition Form</h2>
        <ul class="top-links">
            <li>
                <a href="/Export/country/listCountry?offset=0&max=5" class="btn btn-block btn-primary btn-xs">
                    <i class="fa fa-reorder"></i>
                    Country List
                </a>
            </li>
        </ul>

        <!-- For confirm message-->
        <!-- /.top-links -->
    </section><!-- /.content-header -->

    <g:if test="${flash.message}">
        <section class="flash-message">
            <!-- flash message -->
            <div class="bs-example">
                <div class="alert alert-success small fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong style="font-size: 15px">${flash.message}</strong>
                </div>
            </div>
            <!-- flash message end -->
        </section><!-- /.box-header (Flash Message) -->
    </g:if>

<!-- For Error message-->
    <g:if test="${flash.error}">
        <section class="flash-message">
            <!-- flash message -->
            <div class="bs-example">
                <div class="alert alert-error small fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong style="font-size: 15px">${flash.error}</strong>
                </div>
            </div>
            <!-- flash message end -->
        </section><!-- /.box-header (Flash Message) -->
    </g:if>



    <section class="content"><!-- Main content -->
        <div class="box box-primary" role="main">
            <form action="/Export/country/save" method="post" enctype="multipart/form-data">
                <!--Here--commandName="export" is mandatory for validation message!!!!!!-->
                <div class="box-body">
                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                        <div class="form-group">
                                            <label class="required">Import Country</label>
                                            <select id="importCountryId_0" name="countryName" class="many-to-one form-control" from="exportApplication">
                                                <option value="" selected="selected">Select One</option>
                                                <option value="AFGANISTAN">AFGANISTAN</option>
                                                <option value="ALBANIA">ALBANIA</option>
                                                <option value="ALGERIA">ALGERIA</option>
                                                <option value="AMER SAMOA">AMER SAMOA</option>
                                                <option value="ANDEAN">ANDEAN</option>
                                                <option value="ANDORRA">ANDORRA</option>
                                                <option value="ANGOLA">ANGOLA</option>
                                                <option value="ANTIGUA">ANTIGUA</option>
                                                <option value="ARGENTINA">ARGENTINA</option>
                                                <option value="ARMENIA">ARMENIA</option>
                                                <option value="AUSTRALIA">AUSTRALIA</option>
                                                <option value="AUSTRIA">AUSTRIA</option>
                                                <option value="AZERBAIJAN">AZERBAIJAN</option>
                                                <option value="BAHAMAS">BAHAMAS</option>
                                                <option value="BAHRAIN">BAHRAIN</option>
                                                <option value="BANGLADESH">BANGLADESH</option>
                                                <option value="BARBADOS">BARBADOS</option>
                                                <option value="BELARUS">BELARUS</option>
                                                <option value="BELGIUM">BELGIUM</option>
                                                <option value="BELIZE">BELIZE</option>
                                                <option value="BENIN">BENIN</option>
                                                <option value="BERMUDA">BERMUDA</option>
                                                <option value="BHUTAN">BHUTAN</option>
                                                <option value="BOLIVIA">BOLIVIA</option>
                                                <option value="BOND TO BOND">BOND TO BOND</option>
                                                <option value="BOSNIA">BOSNIA</option>
                                                <option value="BOTSWANA">BOTSWANA</option>
                                                <option value="BR. ANTR. TERR">BR. ANTR. TERR</option>
                                                <option value="BR. IN. OC. TR.">BR. IN. OC. TR.</option>
                                                <option value="BR. VIRGIN. IS">BR. VIRGIN. IS</option>
                                                <option value="BRAZIL">BRAZIL</option>
                                                <option value="BRUNEI">BRUNEI</option>
                                                <option value="BULGARIA">BULGARIA</option>
                                                <option value="BURUNDI">BURUNDI</option>
                                                <option value="CAMEROON">CAMEROON</option>
                                                <option value="CANADA">CANADA</option>
                                                <option value="CANT">CANT. &amp; EN. IS.</option>
                                                <option value="CARE VERDE ISL.">CARE VERDE ISL.</option>
                                                <option value="CAYMAN ISLDS">CAYMAN ISLDS</option>
                                                <option value="CENT. AF. RP.">CENT. AF. RP.</option>
                                                <option value="CEUCA">CEUCA</option>
                                                <option value="CHAD">CHAD</option>
                                                <option value="CHILE">CHILE</option>
                                                <option value="CHINA">CHINA</option>
                                                <option value="COCOS ISLNDS">COCOS ISLNDS</option>
                                                <option value="COLOMBIA">COLOMBIA</option>
                                                <option value="COMBODIA">COMBODIA</option>
                                                <option value="COMORS ISLANDS">COMORS ISLANDS</option>
                                                <option value="CONGO">CONGO</option>
                                                <option value="COSTA RICA">COSTA RICA</option>
                                                <option value="CROATIA">CROATIA</option>
                                                <option value="CUBA">CUBA</option>
                                                <option value="CURACAO">CURACAO</option>
                                                <option value="CYPRUS">CYPRUS</option>
                                                <option value="CZECH REPUBLIC">CZECH REPUBLIC</option>
                                                <option value="CZECHOSLOVAKIA">CZECHOSLOVAKIA</option>
                                                <option value="D.T.A.">D.T.A.</option>
                                                <option value="DENMARK">DENMARK</option>
                                                <option value="DJIBOUTI">DJIBOUTI</option>
                                                <option value="DOMINICA">DOMINICA</option>
                                                <option value="DOMINICAN">DOMINICAN RP</option>
                                                <option value="DUBAI">DUBAI</option>
                                                <option value="EAST TIMO">EAST TIMOR</option>
                                                <option value="ECUADOR">ECUADOR</option>
                                                <option value="EEC">EEC</option>
                                                <option value="EGYPT">EGYPT</option>
                                                <option value="EL SALVADOR">EL SALVADOR</option>
                                                <option value="EQ. GUINEA">EQ. GUINEA</option>
                                                <option value="ESTONIA">ESTONIA</option>
                                                <option value="ETHIOPIA">ETHIOPIA</option>
                                                <option value="FAEROE ISLANDS">FAEROE ISLANDS</option>
                                                <option value="FALKLAND ISL">FALKLAND ISL</option>
                                                <option value="FIJI">FIJI</option>
                                                <option value="FINLAND">FINLAND</option>
                                                <option value="FR. GUIANA">FR. GUIANA</option>
                                                <option value="FR. POLYNESIA">FR. POLYNESIA</option>
                                                <option value="FR. SO. ANT. TR">FR. SO. ANT. TR</option>
                                                <option value="FRANCE">FRANCE</option>
                                                <option value="GABON">GABON</option>
                                                <option value="GAMBIA">GAMBIA</option>
                                                <option value="GEORGIA">GEORGIA</option>
                                                <option value="GERMAN DM RP">GERMAN DM RP</option>
                                                <option value="GERMANY">GERMANY</option>
                                                <option value="GHANA">GHANA</option>
                                                <option value="GIBRALTAR">GIBRALTAR</option>
                                                <option value="GREAT BRITAIN">GREAT BRITAIN</option>
                                                <option value="GREECE">GREECE</option>
                                                <option value="GREENLAND">GREENLAND</option>
                                                <option value="GRENADA">GRENADA</option>
                                                <option value="GUADELOUPE">GUADELOUPE</option>
                                                <option value="GUAM">GUAM</option>
                                                <option value="GUATEMALA">GUATEMALA</option>
                                                <option value="GUINEA">GUINEA</option>
                                                <option value="GUINEABISSAU">GUINEABISSAU</option>
                                                <option value="GUYNA">GUYNA</option>
                                                <option value="HAITI">HAITI</option>
                                                <option value="HOLLAND">HOLLAND</option>
                                                <option value="HONDURAS">HONDURAS</option>
                                                <option value="HONGKONG">HONGKONG</option>
                                                <option value="HUNGARY">HUNGARY</option>
                                                <option value="ICELAND">ICELAND</option>
                                                <option value="INDIA">INDIA</option>
                                                <option value="INDONESIA">INDONESIA</option>
                                                <option value="INTER ZONE">INTER ZONE</option>
                                                <option value="INTRA-ZONE">INTRA-ZONE</option>
                                                <option value="IRAN">IRAN</option>
                                                <option value="IRAQ">IRAQ</option>
                                                <option value="IRELAND">IRELAND</option>
                                                <option value="ISLANDS">ISLANDS</option>
                                                <option value="ISRAEL">ISRAEL</option>
                                                <option value="ITALY">ITALY</option>
                                                <option value="IVORY COAST">IVORY COAST</option>
                                                <option value="JAMICA">JAMICA</option>
                                                <option value="JAPAN">JAPAN</option>
                                                <option value="JORDAN">JORDAN</option>
                                                <option value="KAJAKISTAN">KAJAKISTAN</option>
                                                <option value="KAMPUCHEA DR">KAMPUCHEA DR</option>
                                                <option value="KENYA">KENYA</option>
                                                <option value="KIRIBATI">KIRIBATI</option>
                                                <option value="KOREA D P RP">KOREA D P RP</option>
                                                <option value="KOREA RP">KOREA RP</option>
                                                <option value="KUWAIT">KUWAIT</option>
                                                <option value="LAFTA">LAFTA</option>
                                                <option value="LAFTA N.E.S.">LAFTA N.E.S.</option>
                                                <option value="LAO P.D. REP.">LAO P.D. REP.</option>
                                                <option value="LAOS">LAOS</option>
                                                <option value="LATVIA">LATVIA</option>
                                                <option value="LEBANON">LEBANON</option>
                                                <option value="LESOTHO">LESOTHO</option>
                                                <option value="LIBERIA">LIBERIA</option>
                                                <option value="LIBYA">LIBYA</option>
                                                <option value="LITHUANIA">LITHUANIA</option>
                                                <option value="LOCAL SALE">LOCAL SALE</option>
                                                <option value="LUXEMBOURG">LUXEMBOURG</option>
                                                <option value="MACAU">MACAU</option>
                                                <option value="MACEDONIA">MACEDONIA</option>
                                                <option value="MADAGASCAR">MADAGASCAR</option>
                                                <option value="MALAWI">MALAWI</option>
                                                <option value="MALAYSIA">MALAYSIA</option>
                                                <option value="MALDIVES">MALDIVES</option>
                                                <option value="MALI">MALI</option>
                                                <option value="MALTA">MALTA</option>
                                                <option value="MARSHAL ISLAND">MARSHAL ISLAND</option>
                                                <option value="MARTINIQUE">MARTINIQUE</option>
                                                <option value="MAURITANIA">MAURITANIA</option>
                                                <option value="MAURITIUS">MAURITIUS</option>
                                                <option value="MEXICO">MEXICO</option>
                                                <option value="MIDWAY ISLDS">MIDWAY ISLDS</option>
                                                <option value="MOLDOVA">MOLDOVA</option>
                                                <option value="MONACO">MONACO</option>
                                                <option value="MONGOLIA">MONGOLIA</option>
                                                <option value="MONTSERRAT">MONTSERRAT</option>
                                                <option value="MOROCCO">MOROCCO</option>
                                                <option value="MOZAMBIQUE">MOZAMBIQUE</option>
                                                <option value="Myanmar">Myanmar</option>
                                                <option value="N AFRICA NES">N AFRICA NES</option>
                                                <option value="NAMIBIA">NAMIBIA</option>
                                                <option value="NAURU">NAURU</option>
                                                <option value="NEPAL">NEPAL</option>
                                                <option value="NETH. ANTLES">NETH. ANTLES</option>
                                                <option value="NETHERLAND">NETHERLAND</option>
                                                <option value="NEW CALEDNIA">NEW CALEDNIA</option>
                                                <option value="NEW GUINEA">NEW GUINEA</option>
                                                <option value="NEW HEBRIDES">NEW HEBRIDES</option>
                                                <option value="NEW ZEALAND">NEW ZEALAND</option>
                                                <option value="NHUTAN">NHUTAN</option>
                                                <option value="NICARAGUA">NICARAGUA</option>
                                                <option value="NIGER">NIGER</option>
                                                <option value="NIGERIA">NIGERIA</option>
                                                <option value="NIUE ISLANDS">NIUE ISLANDS</option>
                                                <option value="NORFOLK ISLD">NORFOLK ISLD</option>
                                                <option value="NORWAY">NORWAY</option>
                                                <option value="OMAN">OMAN</option>
                                                <option value="PACEFIC ISLD">PACEFIC ISLD</option>
                                                <option value="PAKISTAN">PAKISTAN</option>
                                                <option value="PANAMA">PANAMA</option>
                                                <option value="PAPUA N. GUIN">PAPUA N. GUIN</option>
                                                <option value="PARAGUAY">PARAGUAY</option>
                                                <option value="PEN. MALAYSIA">PEN. MALAYSIA</option>
                                                <option value="PERU">PERU</option>
                                                <option value="PHILIPPINES">PHILIPPINES</option>
                                                <option value="PITCAIRN ISL">PITCAIRN ISL</option>
                                                <option value="POLAND">POLAND</option>
                                                <option value="PORTUGAL">PORTUGAL</option>
                                                <option value="PUERTO RICCA">PUERTO RICCA</option>
                                                <option value="QATAR">QATAR</option>
                                                <option value="RECONCILE_COUNTRY">RECONCILE_COUNTRY</option>
                                                <option value="REUNION">REUNION</option>
                                                <option value="ROMANIA">ROMANIA</option>
                                                <option value="RUSSIA">RUSSIA</option>
                                                <option value="RWANDA">RWANDA</option>
                                                <option value="RYUKYU IS">RYUKYU IS</option>
                                                <option value="S. KOREA">S. KOREA</option>
                                                <option value="S. VIETAM RP">S. VIETAM RP</option>
                                                <option value="SABAH">SABAH</option>
                                                <option value="SAUDI ARABIA">SAUDI ARABIA</option>
                                                <option value="SCOTLAND">SCOTLAND</option>
                                                <option value="SENEGAL">SENEGAL</option>
                                                <option value="SERVIA">SERVIA</option>
                                                <option value="SEYCHELLES">SEYCHELLES</option>
                                                <option value="SIERRA LEONE">SIERRA LEONE</option>
                                                <option value="SIKKIM">SIKKIM</option>
                                                <option value="SINGAPORE">SINGAPORE</option>
                                                <option value="SLOVAK REPUBLIC">SLOVAK REPUBLIC</option>
                                                <option value="SLOVENIA">SLOVENIA</option>
                                                <option value="SOLOMON ISLD">SOLOMON ISLD</option>
                                                <option value="SOMALIA">SOMALIA</option>
                                                <option value="SOUTH AFRICA">SOUTH AFRICA</option>
                                                <option value="SPAIN">SPAIN</option>
                                                <option value="SRILANKA">SRILANKA</option>
                                                <option value="ST. HELENA">ST. HELENA</option>
                                                <option value="ST. KITTS NEV">ST. KITTS NEV</option>
                                                <option value="ST. PIER. MIQU">ST. PIER. MIQU</option>
                                                <option value="ST. VINCENT">ST. VINCENT</option>
                                                <option value="SUDAN">SUDAN</option>
                                                <option value="SURNAME">SURNAME</option>
                                                <option value="SWAZILAND">SWAZILAND</option>
                                                <option value="SWEDEN">SWEDEN</option>
                                                <option value="SWITZERLAND">SWITZERLAND</option>
                                                <option value="SYRIA">SYRIA</option>
                                                <option value="SYRN ARAB RP">SYRN ARAB RP</option>
                                                <option value="TAIWAN">TAIWAN</option>
                                                <option value="TANZANIA">TANZANIA</option>
                                                <option value="THAI+PAK">THAI+PAK</option>
                                                <option value="THAILAND">THAILAND</option>
                                                <option value="TOGO">TOGO</option>
                                                <option value="TOKRLAU ISLANDS">TOKRLAU ISLANDS</option>
                                                <option value="TONGA">TONGA</option>
                                                <option value="TRINIDAD TBG">TRINIDAD TBG</option>
                                                <option value="TUNISIA">TUNISIA</option>
                                                <option value="TURKEY">TURKEY</option>
                                                <option value="TURKS CA. ISL">TURKS CA. ISL</option>
                                                <option value="TUVALU">TUVALU</option>
                                                <option value="U. A. E.">U. A. E.</option>
                                                <option value="U.S.A.">U.S.A.</option>
                                                <option value="UGANDA">UGANDA</option>
                                                <option value="UJBEKISTAN">UJBEKISTAN</option>
                                                <option value="UKRAINE">UKRAINE</option>
                                                <option value="UNITED KINGDOM">UNITED KINGDOM</option>
                                                <option value="UNTD. RP. CAMR.">UNTD. RP. CAMR.</option>
                                                <option value="UPPER VOLTA">UPPER VOLTA</option>
                                                <option value="URUGUAY">URUGUAY</option>
                                                <option value="US. VIRGIN IS">US. VIRGIN IS</option>
                                                <option value="USSR">USSR</option>
                                                <option value="VANUATU">VANUATU</option>
                                                <option value="VATICAN">VATICAN</option>
                                                <option value="VENEZULEA">VENEZULEA</option>
                                                <option value="VIETNAM">VIETNAM</option>
                                                <option value="WAKE ISLAND">WAKE ISLAND</option>
                                                <option value="WALLIS FUT. I">WALLIS FUT. I</option>
                                                <option value="XHRISTMAS IS">XHRISTMAS IS</option>
                                                <option value="YEMEN">YEMEN</option>
                                                <option value="YUGOSLAVIA">YUGOSLAVIA</option>
                                                <option value="Z_del_MYAN">Z_del_MYAN</option>
                                                <option value="ZAMBIA">ZAMBIA</option>
                                                <option value="ZIMBABWE">ZIMBABWE</option>
                                            </select>
                                            <UIHelper:renderErrorMessage fieldName="importCountry" model="${export}" errorMessage="export.field.blank.message"/>
                                        </div>
                                    </div>
                <div class="box-footer">

                    &nbsp;
                    <button type="submit" name="save" class="btn btn-info" id="update"><i
                            class="fa fa-save"></i>Save Country
                    </button>

                    <button type="reset" class="btn btn-warning pull-right"><i
                            class="fa fa-hand-paper-o"></i>Reset the Form
                    </button>
                </div>

                <form>



<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 3.2.1
    </div>

</footer>


<!--Date picker link-->
<%--<script type="text/javascript" src="<c:url value="/folder/mycssresource/datepicker/js/bootstrap-datepicker.min.js"/>"></script>--%>
<!-- Vendor JS-->
<asset:javascript src="/folder/userResources/vendor/datepicker/moment.min.js"></asset:javascript>
<asset:javascript src="/folder/userResources/vendor/datepicker/daterangepicker.js"></asset:javascript>
<!-- Main JS-->
<asset:javascript src="/folder/userResources/js/global.js"></asset:javascript>



</body>
</html>
