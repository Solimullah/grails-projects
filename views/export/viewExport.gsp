<%--
  Created by IntelliJ IDEA.
  User: Solimullah&Amena
  Date: 12-Sep-19
  Time: 10:49 PM
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html>

<html>
<head>
    <script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script>
    <script async="" src="https://www.google-analytics.com/analytics.js"></script>
    <script async="" src="https://www.googletagmanager.com/gtm.js?id=GTM-MFSJQFR"></script>
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({'gtm.start': new Date().getTime(), event: 'gtm.js'});
            var f = d.getElementsByTagName(s)[0], j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-MFSJQFR');</script>
    <script>(function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
        a = s.createElement(o), m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-91387934-1', 'auto');
    ga('send', 'pageview');</script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Show Dyeing Certificate</title>
    <%--<meta name="layout" content="main">--%>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    <!--style sheet link-->
    <asset:link rel="stylesheet" type="text/css" href="/folder/mycssresource/mycss.css"/>
    <asset:link rel="stylesheet" type="text/css" href="/folder/mycssresources/export/css/AdminLTE.min.css"/>
    <!--Date picker link-->
    <!-- Vendor CSS-->
    <asset:link rel="stylesheet" type="text/css" href="/folder/userResources/vendor/datepicker/daterangepicker.css"/>

    <!--for <i class="fa fa-reorder"></i> teg link-->
    <%--
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    --%>
    <!-- Or this Icons font CSS-->
    <asset:link rel="stylesheet" type="text/css"
                href="/folder/userResources/vendor/font-awesome-4.7/css/font-awesome.min.css"/>

</head>

<body class="skin-blue sidebar-mini sidebar-collapse pace-done">
<div class="content-wrapper custom_mergin" style="min-height: 857px">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3><a href="/Export/">
            <img src="/Export/assets/bepzaLogo.png" height="50px" width="60px">
        </a>Export Application Detail</h3>
        <ul class="top-links">
            <li>
                <a href="/Export/export/listExport?offset=0&max=5" class="btn btn-block btn-primary btn-xs">
                    <i class="fa fa-reorder"></i> Export Application List
                </a>
            </li>
        </ul><!-- /.top-links -->
    </section><!-- /.content-header -->

    <section class="flash-message">
        <!-- flash message -->

        <!-- flash message end -->
    </section><!-- /.box-header (Flash Message) -->

    <section class="content"><!-- Main content -->
        <div class="box box-primary">
            <div class="box-body">

                <fieldset id="showDcMstDyingCertificate">
                    <legend class="coloring2">Basic Information</legend>

                    <div class="box box-info">
                        <div class="box-head">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <h4 class="text-bold">

                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="showTable" style="overflow-x: auto">

                                <table class="table table-bordered table-striped table-hover table-condensed dt-responsive display nowrap">
                                    <tbody><tr>

                                        <th>
                                            <span id="dcZone-label" class="property-label">Zone:</span>
                                        </th>
                                        <td colspan="3">
                                            Cumilla Export Processing Zone
                                        </td>
                                        <th>
                                            <span id="dcEnterprise-label" class="property-label">Enterprise:</span>
                                        </th>
                                        <td colspan="3">
                                            Nassa Spinning Ltd.
                                        </td>

                                    </tr>
                                    <tr>
                                        <th>
                                            <span id="invoiceNo-label" class="property-label">Invoice No:</span>
                                        </th>
                                        <td>
                                            ${export.invoiceNo}
                                        </td>
                                        <th>
                                            <span id="invoiceDate-label" class="property-label">Invoice Date:</span>
                                        </th>
                                        <td>
                                            ${export.invoiceDate}
                                        </td>

                                        <th>
                                            <span id="lcNo-label" class="property-label">LC No:</span>
                                        </th>
                                        <td>
                                            ${export.lcNo}
                                        </td>
                                        <th>
                                            <span id="lcDate-label" class="property-label">LC Date:</span>
                                        </th>
                                        <td>
                                            <span class="property-value"
                                                  aria-labelledby="lcDate-label">${export.lcDate}</span>
                                        </td>

                                    </tr>

                                    <tr>
                                        <th>
                                            <span id="exportPermitNo-label"
                                                  class="property-label">Export Permit No.:</span>
                                        </th>
                                        <td>

                                            <a href="/bepza/exportPermit/show/2871384"
                                               target="_blank">${export.exPermitNo}</a>

                                        </td>
                                        <th>
                                            <span id="exportPermitDate-label"
                                                  class="property-label">Export Permit Date:</span>
                                        </th>
                                        <td>
                                            ${export.exPermitDate}
                                        </td>
                                        <th>
                                            <span id="currentStatus-label" class="property-label">Current Status:</span>
                                        </th>
                                        <td>
                                            Regret
                                        </td>
                                        <th>
                                            <span id="previousStatus-label"
                                                  class="property-label">Previous Status:</span>
                                        </th>
                                        <td>
                                            Checked &amp; Submitted
                                        </td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </fieldset>

                <g:each in="${export.epExportMaterial}" var="eXpMaterial">
                    <fieldset id="showDcDtlExportItem">
                        <legend class="coloring2">Export Item Details</legend>

                        <div class="box box-info">
                            <div class="box-body table-responsive">
                                <div class="showTable" style="overflow-x: auto">
                                    <table class="table table-bordered table-striped table-hover table-condensed dt-responsive display nowrap">
                                        <thead style="background-color: lightblue">
                                        <tr>

                                            <th>Item Name</th>

                                            <th>Item Qty</th>

                                            <th>Unit Of Qty</th>

                                            <th>Consignee Name</th>

                                            <th>Consignee Address</th>

                                        </tr>
                                        </thead>
                                        <tbody>

                                        <tr class="even">

                                            <td>${eXpMaterial.exItemName}</td>

                                            <td>${eXpMaterial.exItemQty}</td>

                                            <td>${eXpMaterial.exUnitOfQty}</td>

                                            <td>${eXpMaterial.consigneeName}</td>

                                            <td>${eXpMaterial.consigneeAddress}</td>

                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </fieldset>
                </g:each>
                <fieldset id="showDcDtlImportItem">
                    <legend class="coloring2">Import Item Details</legend>

                    <div class="box box-info">
                        <div class="box-body table-responsive">
                            <div class="showTable" style="overflow-x: auto">
                                <table class="table table-bordered table-striped table-hover table-condensed dt-responsive display nowrap">
                                    <thead style="background-color: lightblue">
                                    <tr>
                                        <th>Item Name</th>

                                        <th>Item Qty</th>

                                        <th>Import Country</th>

                                        <th>Import Permit No</th>

                                        <th>Import Permit Date</th>

                                        <th>Bill of Entry No</th>

                                        <th>Bill of Entry Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr class="even">

                                        <td>${export.ipItemName}</td>

                                        <td>${export.ipItemQty}${export.ipUnitOfQty}</td>

                                        <td>${export.importCountry}</td>

                                        <td>${export.ipNo}</td>

                                        <td>${export.ipDate}</td>

                                        <td>${export.billOfEntryNo}</td>

                                        <td>${export.billOfEntryDate}</td>

                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <fieldset id="showRpDtlSupportingDoc">
                    <legend class="coloring2">Supporting Documents (Attachments)</legend>

                    <div class="box box-info">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered table-striped table-hover table-condensed dt-responsive display nowrap">
                                        <thead style="background-color: lightblue">
                                        <tr>
                                            <th style="width: 10px">#</th>
                                            <th>Supporting Documents Type</th>
                                            <th>Attached Documents</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <g:each in="${export.attachments}" var="pdfDocs">
                                            <g:if test="${pdfDocs.docId.equals('6')}"><%-- Coutaion ('') cause- here 6 is a String value...--%>
                                                <tr>
                                                    <td>
                                                        1
                                                    </td>

                                                    <td>Import Documents</td>

                                                    <td>
                                                        <a href="/Export/myFolder/exportfiles/${pdfDocs.files}"
                                                           target="_blank"><i class="fa fa-fw fa-file-pdf-o"></i></a>
                                                    </td>
                                                </tr>
                                            </g:if>

                                            <g:if test="${pdfDocs.docId.equals('3')}">
                                                <tr>
                                                    <td>
                                                        2
                                                    </td>
                                                    <td>Consumption Certification</td>
                                                    <td><a href="/Export/myFolder/exportfiles/${pdfDocs.files}"
                                                           target="_blank"><i class="fa fa-fw fa-file-pdf-o"></i></a>
                                                    </td>
                                                </tr>
                                            </g:if>

                                            <g:if test="${pdfDocs.docId.equals('4')}">
                                                <tr>
                                                    <td>
                                                        3
                                                    </td>
                                                    <td>Delivery Challan</td>
                                                    <td><a href="/Export/myFolder/exportfiles/${pdfDocs.files}"
                                                           target="_blank"><i class="fa fa-fw fa-file-pdf-o"></i></a>
                                                    </td>
                                                </tr>
                                            </g:if>

                                            <g:if test="${pdfDocs.docId.equals('1')}">
                                                <tr>
                                                    <td>
                                                        4
                                                    </td>
                                                    <td>Commercial Invoice</td>
                                                    <td><a href="/Export/myFolder/exportfiles/${pdfDocs.files}"
                                                           target="_blank"><i class="fa fa-fw fa-file-pdf-o"></i></a>
                                                    </td>
                                                </tr>
                                            </g:if>

                                            <g:if test="${pdfDocs.docId.equals('0')}">
                                                <tr>
                                                    <td>
                                                        5
                                                    </td>
                                                    <td>Master LC</td>
                                                    <td><a href="/Export/myFolder/exportfiles/${pdfDocs.files}"
                                                           target="_blank"><i class="fa fa-fw fa-file-pdf-o"></i></a>
                                                    </td>
                                                </tr>
                                            </g:if>

                                            <g:if test="${pdfDocs.docId.equals('5')}">
                                                <tr>
                                                    <td>
                                                        6
                                                    </td>
                                                    <td>Export Documents</td>
                                                    <td><a href="/Export/myFolder/exportfiles/${pdfDocs.files}"
                                                           target="_blank"><i class="fa fa-fw fa-file-pdf-o"></i></a>
                                                    </td>
                                                </tr>
                                            </g:if>

                                            <g:if test="${pdfDocs.docId.equals('2')}">
                                                <tr>
                                                    <td>
                                                        7
                                                    </td>
                                                    <td>Others</td>
                                                    <td><a href="/Export/myFolder/exportfiles/${pdfDocs.files}"
                                                           target="_blank"><i class="fa fa-fw fa-file-pdf-o"></i></a>
                                                    </td>
                                                </tr>
                                            </g:if>
                                        </g:each>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </fieldset>

                <fieldset><legend class="coloring2">Application Process History</legend>

                    <div class="box box-info">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-hover">
                                        <thead style="background-color: lightblue">
                                        <tr>
                                            <th>#</th>
                                            <th>On Desk</th>
                                            <th>Processed By</th>
                                            <th>Status</th>
                                            <th>Process Time</th>
                                            <th>Remarks</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <tr>
                                            <td>1</td>
                                            <td>Enterprise - Maker</td>
                                            <td>Dipankar Deb Nath</td>
                                            <td>Saved as Draft</td>
                                            <td>18/04/2018 12:19:23 PM</td>
                                            <td></td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </fieldset>


                <span class="pull-left"><label>Application Tracking No. #</label><i
                        class="text-green">&nbsp;DE180418000008</i></span>

            </div><!-- /.box-body -->
        </div>  <!-- /.box box-primary -->
    </section><!-- /.content -->
</div>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 3.2.4
    </div>

</footer>


<script type="text/javascript">
    var DATE_FORMAT = "dd/mm/yyyy";
    var DATE_FORMAT_JQ = "dd/mm/yy";
    var DOB_YEAR_RANGE = "-100:+0";
    var contextPath = "/bepza";
    jQuery(document).ready(function () {
        contextPath = "/bepza";
        $('.cb').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            increaseArea: '70%' // optional
        });
        $('.rb').iCheck({
            radioClass: 'iradio_square-blue',
            increaseArea: '70%' // optional
        });
    });
    //==========================================================
    // file name for user edit profile
    // $(".files").change(function () {
    $(document).on("change", ".files", function () {
        var value = this.value;
        if (value) {
            $(".docNameProxy").val(value);
        }
    });
    //***********************************************************
    //============================================================
    // Doc name for commercial module
    $(".requiredDocFile").change(function () {
        var id = this.id;
        var value = this.value;
        var idSegments = id.split("_");
        var rowId = idSegments[1];
        if (value) {
            $("#requiredDocId_" + rowId).prop("checked", true);
            $("#fileName_" + rowId).val(value);
        }
    });
    //************************************************************
    //=============================================================
    //Date picker for commercial module
    $('.datePickForSearch').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        todayHighlight: true

    });
    //******************************************************
    //=======================================================
    // collaper action for ep ip scp process & view page
    $(".collapse-btn").click(function () {
        var _this = this;
        $(_this).closest("legend").next(".collapse-block").slideToggle("slow", function () {
            $(_this).find("i").toggleClass("fa fa-minus fa fa-plus");
        });
    });
    //**************************************************
    //==================================================
    //form action for commercial module
    $("button#submit").click(function () {
        $("#submitType").val("submit");
        $("form#checkForm").submit();
    });
    $("button#update").click(function () {
        $("#submitType").val("update");
        $("form#checkForm").submit();
    });
    $("button.sendBack").click(function () {
        $('#sendBackReason').prop('disabled', false);
        var sendBackReason = $('#sendBackReason').val();
        if (sendBackReason) {
            $("#submitType").val("sendBack");
            $("#checkForm").submit();
        }
    });
    $("#sendBackReason").focusout(function () {
        $('#sendBackReason').prop('disabled', true);
    });

    //**********************************************//
</script>

</body>
</html>
