<%--
  Created by IntelliJ IDEA.
  User: Solimullah&Amena
  Date: 10-Sep-19
  Time: 5:55 PM
--%>

<%--
    Document   : exportform
    Created on : Jan 5, 2019, 12:09:53 PM
    Author     : Solimullah&Amena
--%>

<%@ page import="com.me.grailsmvc.Documents; com.me.grailsmvc.Country" contentType="text/html" pageEncoding="UTF-8" %>
<%--
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
--%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Create Dyeing Certificate</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    <!--style sheet link-->
    <asset:link rel="stylesheet" type="text/css" href="/folder/mycssresource/mycss.css"/>
    <asset:link rel="stylesheet" type="text/css" href="/folder/mycssresource/export/css/AdminLTE.min.css"/>
    <!--Date picker link-->
    <!-- Vendor CSS-->
    <asset:link href="/folder/userResources/vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all"/>

    <!--for <i class="fa fa-reorder"></i> teg link-->

    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">-->

    <!-- Or this Icons font CSS-->
    <asset:link href="/folder/userResources/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet"
                media="all"/>


</head>


<body class="skin-blue sidebar-mini sidebar-collapse  pace-done">

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper custom_mergin">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h2><a href="/Export/">
            <img src="/Export/assets/bepzaLogo.png" height="50px" width="60px">
        </a>Export Application Form</h2>
        <ul class="top-links">
            <li>
                <a href="/Export/export/listExport?offset=0&max=5" class="btn btn-block btn-primary btn-xs">
                    <i class="fa fa-reorder"></i>
                    Export Application List
                </a>
            </li>
        </ul>

        <!-- For confirm message-->
        <!-- /.top-links -->
    </section><!-- /.content-header -->

    <g:if test="${flash.message}">
        <section class="flash-message">
            <!-- flash message -->
            <div class="bs-example">
                <div class="alert alert-success small fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong style="font-size: 15px">${flash.message}</strong>
                </div>
            </div>
            <!-- flash message end -->
        </section><!-- /.box-header (Flash Message) -->
    </g:if>

<!-- For Error message-->
    <g:if test="${flash.error}">
        <section class="flash-message">
            <!-- flash message -->
            <div class="bs-example">
                <div class="alert alert-error small fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong style="font-size: 15px">${flash.error}</strong>
                </div>
            </div>
            <!-- flash message end -->
        </section><!-- /.box-header (Flash Message) -->
    </g:if>



    <section class="content"><!-- Main content -->
        <div class="box box-primary" role="main">
            <form action="/Export/export/save" method="post" enctype="multipart/form-data">
                <!--Here--commandName="export" is mandatory for validation message!!!!!!-->
                <div class="box-body">
                    <input type="hidden" name="actionType" id="submitType" value="">

                    <fieldset class="form">
                        <div class="row">
                            <div class="col-md-3 ">
                                <div class="form-group">
                                    <label class="required">Zone</label>

                                    <select id="dcZone" name="zoneName" class="form-control" disabled="disabled"
                                            from="exportApplication">
                                        <option value="">Select One</option>
                                        <option value="Cumilla Export Processing Zone"
                                                selected="selected">Cumilla Export Processing Zone</option>
                                    </select>
                                    <%--For Validation--%>
                                    <input type="hidden" name="DyeingCertificate.dcZone.id" value="7"
                                           id="DyeingCertificate.dcZone.id">
                                </div>
                            </div>

                            <div class="col-md-3 ">
                                <div class="form-group">
                                    <label class="required">Enterprise</label>

                                    <select id="dcEnterprise" name="entprzName" class="form-control" disabled="disabled"
                                            from="exportApplication">
                                        <option value="">Select One</option>
                                        <option value="Nassa Spining Ltd."
                                                selected="selected">Nassa Spining Ltd.</option>
                                    </select>
                                    <!--For Validation-->
                                    <input type="hidden" name="DyeingCertificate.dcEnterprise.id" value="1768"
                                           id="DyeingCertificate.dcEnterprise.id">

                                </div>
                            </div>

                            <div class="col-md-3 ">
                                <div class="form-group">
                                    <label class="required">Export Permit No</label><label></label>
                                    <input type="text" name="exPermitNo" class="form-control"
                                           id="DyeingCertificate.exportPermitNo" value="${export.exPermitNo}"/>
                                    <UIHelper:renderErrorMessage fieldName="exPermitNo" model="${export}"
                                                                 errorMessage="export.field.blank.message"/>
                                </div>
                            </div>

                            <div class="col-md-3 ">
                                <div class="form-group te">
                                    <label class="required">Export Permit Date</label>
                                    <input type="text" name="exPermitDate" id="exportPermitDate" value=""
                                           class="form-control js-datepicker" placeholder="dd/mm/yyyy"/>
                                    <UIHelper:renderErrorMessage fieldName="exPermitDate" model="${export}"
                                                                 errorMessage="export.field.blank.message"/>
                                    <UIHelper:renderErrorMessage fieldName="exPermitDate" model="${export}"
                                                                 errorMessage="and date format is dd/mm/yyyy"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 ">
                                <div class="form-group">
                                    <label class="required">Commercial Invoice No</label>
                                    <input type="text" name="invoiceNo" class="form-control"
                                           id="DyeingCertificate.invoiceNo" value=""/>
                                    <UIHelper:renderErrorMessage fieldName="invoiceNo" model="${export}"
                                                                 errorMessage="export.field.blank.message"/>
                                </div>
                            </div>

                            <div class="col-md-3 ">
                                <div class="form-group">
                                    <label class="required">Invoice Date</label>
                                    <input type="text" name="invoiceDate" id="invoiceDate" value=""
                                           class="form-control js-datepicker" placeholder="dd/mm/yyyy"/>
                                    <UIHelper:renderErrorMessage fieldName="invoiceDate" model="${export}"
                                                                 errorMessage="export.field.blank.message"/>
                                    <UIHelper:renderErrorMessage fieldName="invoiceDate" model="${export}"
                                                                 errorMessage="and date format is dd/mm/yyyy"/>
                                </div>
                            </div>

                            <div class="col-md-3  ">
                                <div class="form-group">
                                    <label>LC/SC No.</label>
                                    <input type="text" name="lcNo" class="form-control" id="DyeingCertificate.lcNo"
                                           value=""/>

                                </div>
                            </div>

                            <div class="col-md-3 ">
                                <div class="form-group">
                                    <label>LC/SC Date</label>
                                    <input type="text" name="lcDate" id="lcDate" value=""
                                           class="form-control js-datepicker" placeholder="dd/mm/yyyy"/>

                                </div>
                            </div>
                        </div>
                    </fieldset>


                    <fieldset>
                        <legend class="coloring2">Export Item Details</legend>

                        <div class="box box-info epExportMaterial">
                            <div class="box-body">
                                <div class="row">

                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                        <div class="form-group">
                                            <label class="required">Item Name</label>
                                            <input type="text" name="EpExportMaterial[0].exItemName" id="itemName_0"
                                                   class="form-control"/>
                                            <UIHelper:renderErrorMessage fieldName="exItemName"
                                                                         model="${epExportMaterial}"
                                                                         errorMessage="export.field.blank.message"/>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                        <div class="form-group">
                                            <label class="required">Item Qty</label>
                                            <input type="number" name="EpExportMaterial[0].exItemQty" id="itemQty_0"
                                                   class="form-control"/>
                                            <UIHelper:renderErrorMessage fieldName="exItemQty"
                                                                         model="${epExportMaterial}"
                                                                         errorMessage="export.field.blank.message"/>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                        <div class="form-group">
                                            <label class="required">Unit Of Qty</label>
                                            <select id="unitOfQtyId_0" name="EpExportMaterial[0].exUnitOfQty"
                                                    class="many-to-one form-control" from="exportApplication">
                                                <option value="" selected="selected">Select One</option>
                                                <option value="Skid">Skid</option>
                                                <option value="Tons">Tons</option>
                                                <option value="case">case</option>
                                                <option value="Truck">Truck</option>
                                                <option value="Nos">Nos</option>
                                                <option value="Pail">Pail</option>
                                                <option value="Unit">Unit</option>
                                                <option value="Gallon">Gallon</option>
                                                <option value="Cans">Cans</option>
                                                <option value="BUNDLE">BUNDLE</option>
                                                <option value="Bags">Bags</option>
                                                <option value="Bales">Bales</option>
                                                <option value="Box">Box</option>
                                                <option value="Carton">Carton</option>
                                                <option value="CM">CM</option>
                                                <option value="Cones">Cones</option>
                                                <option value="CRATE">CRATE</option>
                                                <option value="ctns">ctns</option>
                                                <option value="dozon">dozon</option>
                                                <option value="Drum">Drum</option>
                                                <option value="Feet">Feet</option>
                                                <option value="GG">GG</option>
                                                <option value="gm">gm</option>
                                                <option value="Gross">Gross</option>
                                                <option value="Gyds">Gyds</option>
                                                <option value="KG">KG</option>
                                                <option value="KG">KG(Approx.)</option>
                                                <option value="Lbs">Lbs</option>
                                                <option value="Litre">Litre</option>
                                                <option value="Van">Van</option>
                                                <option value="Meter">Meter</option>
                                                <option value="MT">MT</option>
                                                <option value="mtrs">mtrs</option>
                                                <option value="Packages">Packages</option>
                                                <option value="Pair">Pair</option>
                                                <option value="Pallet">Pallet</option>
                                                <option value="Pcs">Pcs</option>
                                                <option value="Cut Pcs">Cut Pcs</option>
                                                <option value="Ream">Ream</option>
                                                <option value="Reel">Reel</option>
                                                <option value="roll">roll</option>
                                                <option value="Set">Set</option>
                                                <option value="SFTS">SFTS</option>
                                                <option value="Sheet">Sheet</option>
                                                <option value="Square Feet">Square Feet</option>
                                                <option value="Square Meter">Square Meter</option>
                                                <option value="Case">Wooden Case</option>
                                                <option value="YDS">YDS</option>
                                                <option value="Lot">Lot</option>
                                            </select>
                                            <UIHelper:renderErrorMessage fieldName="exUnitOfQty"
                                                                         model="${epExportMaterial}"
                                                                         errorMessage="export.field.blank.message"/>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 ">
                                        <div class="form-group">
                                            <label>Consignee Name</label>
                                            <input type="text" name="EpExportMaterial[0].consigneeName"
                                                   id="consigneeName_0" class="form-control"/>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 ">
                                        <div class="form-group">
                                            <label>Consignee Address</label>
                                            <input type="text" name="EpExportMaterial[0].consigneeAddress"
                                                   id="consigneeAddress_0" class="form-control"/>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="pull-right">
                            <a class="btn btn-block btn-primary btn-xs" onclick="addMore('epExportMaterial');">
                                <i class="fa fa-plus">Add more</i>
                            </a>
                        </div>
                    </fieldset>


                    <fieldset>
                        <legend class="coloring2">Import Item Details</legend>


                        <div class="box box-info dcDtlImportItemInstance">
                            <div class="box-body">
                                <div class="row">

                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                        <div class="form-group">
                                            <label class="required">Item Name</label>
                                            <input type="text" name="ipItemName" id="itemName" class="form-control"/>
                                            <UIHelper:renderErrorMessage fieldName="ipItemName" model="${export}"
                                                                         errorMessage="export.field.blank.message"/>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                        <div class="form-group">
                                            <label class="required">Item Qty</label>
                                            <input type="number" name="ipItemQty" id="itemQty" min="0"
                                                   class="form-control"/>
                                            <UIHelper:renderErrorMessage fieldName="ipItemQty" model="${export}"
                                                                         errorMessage="export.field.blank.message"/>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                        <div class="form-group">
                                            <label class="required">Unit Of Qty</label>
                                            <select id="unitOfQtyId" name="ipUnitOfQty" class="many-to-one form-control"
                                                    from="exportApplication">
                                                <option value="" selected="selected">Select One</option>
                                                <option value="Skid">Skid</option>
                                                <option value="Tons">Tons</option>
                                                <option value="case">case</option>
                                                <option value="Truck">Truck</option>
                                                <option value="Nos">Nos</option>
                                                <option value="Pail">Pail</option>
                                                <option value="Unit">Unit</option>
                                                <option value="Gallon">Gallon</option>
                                                <option value="Cans">Cans</option>
                                                <option value="BUNDLE">BUNDLE</option>
                                                <option value="Bags">Bags</option>
                                                <option value="Bales">Bales</option>
                                                <option value="Box">Box</option>
                                                <option value="Carton">Carton</option>
                                                <option value="CM">CM</option>
                                                <option value="Cones">Cones</option>
                                                <option value="CRATE">CRATE</option>
                                                <option value="ctns">ctns</option>
                                                <option value="dozon">dozon</option>
                                                <option value="Drum">Drum</option>
                                                <option value="Feet">Feet</option>
                                                <option value="GG">GG</option>
                                                <option value="gm">gm</option>
                                                <option value="Gross">Gross</option>
                                                <option value="Gyds">Gyds</option>
                                                <option value="KG">KG</option>
                                                <option value="KG (Approx.)">KG (Approx.)</option>
                                                <option value="Lbs">Lbs</option>
                                                <option value="Litre">Litre</option>
                                                <option value="Van">Van</option>
                                                <option value="Meter">Meter</option>
                                                <option value="MT">MT</option>
                                                <option value="mtrs">mtrs</option>
                                                <option value="Packages">Packages</option>
                                                <option value="Pair">Pair</option>
                                                <option value="Pallet">Pallet</option>
                                                <option value="Pcs">Pcs</option>
                                                <option value="Cut Pcs">Cut Pcs</option>
                                                <option value="Ream">Ream</option>
                                                <option value="Reel">Reel</option>
                                                <option value="roll">roll</option>
                                                <option value="Set">Set</option>
                                                <option value="SFTS">SFTS</option>
                                                <option value="Sheet">Sheet</option>
                                                <option value="Square Feet">Square Feet</option>
                                                <option value="Square Meter">Square Meter</option>
                                                <option value="Wooden Case">Wooden Case</option>
                                                <option value="YDS">YDS</option>
                                                <option value="Lot">Lot</option>
                                            </select>
                                            <UIHelper:renderErrorMessage fieldName="ipUnitOfQty" model="${export}"
                                                                         errorMessage="export.field.blank.message"/>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                        <div class="form-group">
                                            <label class="required">Import Country</label>
                                            <g:select name="importCountry" from="${Country.findAll()}"
                                                      optionValue="countryName" optionKey="countryName"
                                                      class="many-to-one form-control"/>
                                        </div>
                                    </div>


                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 ">
                                        <div class="form-group">
                                            <label class="required">Import Permit No</label>
                                            <input type="text" id="ipNo_0" name="ipNo" class="form-control"/>
                                            <UIHelper:renderErrorMessage fieldName="ipNo" model="${export}"
                                                                         errorMessage="export.field.blank.message"/>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 ">
                                        <div class="form-group">
                                            <label class="required">Import Permit Date</label>
                                            <input type="text" id="ipDate_0" name="ipDate" value=""
                                                   class="form-control js-datepicker" placeholder="dd/mm/yyyy"/>
                                            <UIHelper:renderErrorMessage fieldName="ipDate" model="${export}"
                                                                         errorMessage="export.field.blank.message"/>
                                            <UIHelper:renderErrorMessage fieldName="ipDate" model="${export}"
                                                                         errorMessage="and date format is dd/mm/yyyy"/>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 ">
                                        <div class="form-group">
                                            <label class="required">Bill of Entry No</label>
                                            <input type="text" id="billOfEntryNo_0" name="billOfEntryNo"
                                                   class="form-control"/>
                                            <UIHelper:renderErrorMessage fieldName="billOfEntryNo" model="${export}"
                                                                         errorMessage="export.field.blank.message"/>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 ">
                                        <div class="form-group">
                                            <label class="required">Bill of Entry Date</label>
                                            <input type="text" id="billOfEntryDate_0" name="billOfEntryDate" value=""
                                                   class="form-control js-datepicker" placeholder="dd/mm/yyyy"/>
                                            <UIHelper:renderErrorMessage fieldName="billOfEntryDate" model="${export}"
                                                                         errorMessage="export.field.blank.message"/>
                                            <UIHelper:renderErrorMessage fieldName="billOfEntryDate" model="${export}"
                                                                         errorMessage="and date format is dd/mm/yyyy"/>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="pull-right">
                            <a class="btn btn-block btn-primary btn-xs" onclick="addMore('dcDtlImportItemInstance');">
                                <i class="fa fa-plus"></i> Add more
                            </a>
                        </div>
                    </fieldset>


                    <fieldset>
                        <legend class="text-yellow">Supporting Documents (Attachments)</legend>

                        <div class="box box-info">
                            <div class="box-body">
                                <table class="table table-condensed">
                                    <tbody>
                                    <tr>
                                        <th style="width: 10px">#</th>
                                        <th>Supporting Documents Type</th>

                                        <th>Uploaded Documents</th>

                                        <th>Attach Related Supporting Documents (*.pdf) (File size max. 1MB)</th>
                                    </tr>


                                    <g:each in="${documents}" var="docType" status="j">
                                        %{--<g:if test="${docType.id.equals(1)}"><%-- No coutaion ('') cause- here 1 is an Integer value...--%>--}%
                                            <tr>
                                                <td>

                                                    <input type="checkbox" name="Attachments[${j}].isActive"
                                                           id="requiredDocId_${j}"/>
                                                    <input type="hidden" name="Attachments[${j}].isActive" value="false"/>
                                                    <input type="hidden" name="Attachments[${j}].docId" value="${j}"
                                                           id="DcDtlSupportingDoc[${j}].docId"/>
                                                    <input type="hidden" name="DcDtlSupportingDoc[${j}].id" value=""
                                                           id="DcDtlSupportingDoc[${j}].id"/>
                                                </td>

                                                <td>

                                                    <label class="optional">${docType.docName}</label>

                                                    <%--<label class="optional">${documents.docName[0]}</label>--%>
                                                </td>


                                                <td>

                                                </td>

                                                <td>
                                                    <div class="input-group ">
                                                        <span class="input-group-btn">
                                                            <span class="btn btn-primary btn-file">
                                                                Browse …
                                                                <input type="file" id="docName_${j}"
                                                                       name="Attachments[${j}].files"
                                                                       class="requiredDocFile" value=""
                                                                       accept="application/pdf,application/vnd.ms-excel,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"/>
                                                            </span>
                                                        </span>
                                                        <input type="text" class="form-control" readonly="readonly"
                                                               id="fileName_${j}" placeholder="Browse your file" value=""
                                                               name=""/>

                                                        <input type="hidden" name="DcDtlSupportingDo[${j}].docNameOld"
                                                               value="" id="DcDtlSupportingDoc[${j}].docNameOld"/>
                                                        <input type="hidden" name="DcDtlSupportingDo[${j}].docSizeOld"
                                                               value="" id="DcDtlSupportingDoc[${j}].docSizeOld"/>
                                                    </div>
                                                </td>
                                            </tr>
                                        %{--</g:if>

                                        <g:if test="${docType.id.equals(2)}">

                                            <tr>
                                                <td>

                                                    <input type="hidden" name="DcDtlSupportingDo[1]._requiredDocId"/>
                                                    <input type="checkbox" name="Attachments[1].isActive"
                                                           id="requiredDocId_1"/>
                                                    <input type="hidden" name="Attachments[1].isActive" value="false"/>
                                                    <input type="hidden" name="Attachments[1].docId" value="1"
                                                           id="DcDtlSupportingDoc[1].typeId"/>
                                                    <input type="hidden" name="DcDtlSupportingDo[1].id" value=""
                                                           id="DcDtlSupportingDoc[1].id"/>
                                                </td>

                                                <td>

                                                    <label class="optional">${docType.docName}</label>

                                                    <%--<label class="optional">${documents.docName[1]}</label>--%>
                                                </td>


                                                <td>

                                                </td>

                                                <td>
                                                    <div class="input-group ">
                                                        <span class="input-group-btn">
                                                            <span class="btn btn-primary btn-file">
                                                                Browse …
                                                                <input type="file" id="docName_1"
                                                                       name="Attachments[1].files"
                                                                       class="requiredDocFile" value=""
                                                                       accept="application/pdf,application/vnd.ms-excel,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"/>
                                                            </span>
                                                        </span>
                                                        <input type="text" class="form-control" readonly="readonly"
                                                               id="fileName_1" placeholder="Browse your file" value=""
                                                               name=""/>

                                                        <input type="hidden" name="DcDtlSupportingDo[1].docNameOld"
                                                               value="" id="DcDtlSupportingDoc[1].docNameOld"/>
                                                        <input type="hidden" name="DcDtlSupportingDo[1].docSizeOld"
                                                               value="" id="DcDtlSupportingDoc[1].docSizeOld"/>
                                                    </div>
                                                </td>
                                            </tr>
                                        </g:if>
                                        <g:if test="${docType.id.equals(3)}">

                                            <tr>
                                                <td>

                                                    <input type="hidden" name="DcDtlSupportingDo[2]._requiredDocId"/>
                                                    <input type="checkbox" name="Attachments[2].isActive"
                                                           id="requiredDocId_2"/>
                                                    <input type="hidden" name="Attachments[2].isActive" value="false"/>
                                                    <input type="hidden" name="Attachments[2].docId" value="2"
                                                           id="DcDtlSupportingDoc[2].typeId"/>
                                                    <input type="hidden" name="DcDtlSupportingDo[2].id" value=""
                                                           id="DcDtlSupportingDoc[2].id"/>
                                                </td>

                                                <td>

                                                    <label class="optional">${docType.docName}</label>

                                                    <%--<label class="optional">${documents.docName[2]}</label>--%>
                                                </td>


                                                <td>

                                                </td>

                                                <td>
                                                    <div class="input-group ">
                                                        <span class="input-group-btn">
                                                            <span class="btn btn-primary btn-file">
                                                                Browse …
                                                                <input type="file" id="docName_2"
                                                                       name="Attachments[2].files"
                                                                       class="requiredDocFile" value=""
                                                                       accept="application/pdf,application/vnd.ms-excel,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"/>
                                                            </span>
                                                        </span>
                                                        <input type="text" class="form-control" readonly="readonly"
                                                               id="fileName_2" placeholder="Browse your file" value=""
                                                               name=""/>

                                                        <input type="hidden" name="DcDtlSupportingDo[2].docNameOld"
                                                               value="" id="DcDtlSupportingDoc[2].docNameOld"/>
                                                        <input type="hidden" name="DcDtlSupportingDo[2].docSizeOld"
                                                               value="" id="DcDtlSupportingDoc[2].docSizeOld"/>
                                                    </div>
                                                </td>
                                            </tr>
                                        </g:if>
                                        <g:if test="${docType.id.equals(4)}">

                                            <tr>
                                                <td>

                                                    <input type="hidden" name="DcDtlSupportingDo[3]._requiredDocId"/>
                                                    <input type="checkbox" name="Attachments[3].isActive"
                                                           id="requiredDocId_3"/>
                                                    <input type="hidden" name="Attachments[3].isActive" value="false"/>
                                                    <input type="hidden" name="Attachments[3].docId" value="3"
                                                           id="DcDtlSupportingDoc[3].typeId"/>
                                                    <input type="hidden" name="DcDtlSupportingDo[3].id" value=""
                                                           id="DcDtlSupportingDoc[3].id"/>
                                                </td>

                                                <td>

                                                    <label class="optional">${docType.docName}</label>

                                                    <%--<label class="optional">${documents.docName[3]}</label>--%>
                                                </td>


                                                <td>

                                                </td>

                                                <td>
                                                    <div class="input-group ">
                                                        <span class="input-group-btn">
                                                            <span class="btn btn-primary btn-file">
                                                                Browse …
                                                                <input type="file" id="docName_3"
                                                                       name="Attachments[3].files"
                                                                       class="requiredDocFile" value=""
                                                                       accept="application/pdf,application/vnd.ms-excel,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"/>
                                                            </span>
                                                        </span>
                                                        <input type="text" class="form-control" readonly="readonly"
                                                               id="fileName_3" placeholder="Browse your file" value=""
                                                               name=""/>

                                                        <input type="hidden" name="DcDtlSupportingDo[3].docNameOld"
                                                               value="" id="DcDtlSupportingDoc[3].docNameOld"/>
                                                        <input type="hidden" name="DcDtlSupportingDo[3].docSizeOld"
                                                               value="" id="DcDtlSupportingDoc[3].docSizeOld"/>
                                                    </div>
                                                </td>
                                            </tr>
                                        </g:if>

                                        <g:if test="${docType.id.equals(5)}">
                                            <tr>
                                                <td>

                                                    <input type="hidden" name="DcDtlSupportingDo[4]._requiredDocId"/>
                                                    <input type="checkbox" name="Attachments[4].isActive"
                                                           id="requiredDocId_4"/>
                                                    <input type="hidden" name="Attachments[4].isActive" value="false"/>
                                                    <input type="hidden" name="Attachments[4].docId" value="4"
                                                           id="DcDtlSupportingDoc[4].typeId"/>
                                                    <input type="hidden" name="DcDtlSupportingDo[4].id" value=""
                                                           id="DcDtlSupportingDoc[4].id"/>
                                                </td>

                                                <td>

                                                    <label class="optional">${docType.docName}</label>

                                                    <%--<label class="optional">${documents.docName[4]}</label>--%>
                                                </td>


                                                <td>

                                                </td>

                                                <td>
                                                    <div class="input-group ">
                                                        <span class="input-group-btn">
                                                            <span class="btn btn-primary btn-file">
                                                                Browse …
                                                                <input type="file" id="docName_4"
                                                                       name="Attachments[4].files"
                                                                       class="requiredDocFile" value=""
                                                                       accept="application/pdf,application/vnd.ms-excel,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document">
                                                            </span>
                                                        </span>
                                                        <input type="text" class="form-control" readonly="readonly"
                                                               id="fileName_4" placeholder="Browse your file" value=""
                                                               name=""/>

                                                        <input type="hidden" name="DcDtlSupportingDo[4].docNameOld"
                                                               value="" id="DcDtlSupportingDoc[4].docNameOld"/>
                                                        <input type="hidden" name="DcDtlSupportingDo[4].docSizeOld"
                                                               value="" id="DcDtlSupportingDoc[4].docSizeOld"/>
                                                    </div>
                                                </td>
                                            </tr>
                                        </g:if>

                                        <g:if test="${docType.id.equals(6)}">
                                            <tr>
                                                <td>

                                                    <input type="hidden" name="DcDtlSupportingDo[5]._requiredDocId">
                                                    <input type="checkbox" name="Attachments[5].isActive"
                                                           id="requiredDocId_5">
                                                    <input type="hidden" name="Attachments[5].isActive" value="false"/>
                                                    <input type="hidden" name="Attachments[5].docId" value="5"
                                                           id="DcDtlSupportingDoc[5].typeId">
                                                    <input type="hidden" name="DcDtlSupportingDo[5].id" value=""
                                                           id="DcDtlSupportingDoc[5].id">
                                                </td>

                                                <td>

                                                    <label class="optional">${docType.docName}</label>

                                                    <%--<label class="optional">${documents.docName[5]}</label>--%>
                                                </td>


                                                <td>

                                                </td>

                                                <td>
                                                    <div class="input-group ">
                                                        <span class="input-group-btn">
                                                            <span class="btn btn-primary btn-file">
                                                                Browse …
                                                                <input type="file" id="docName_5"
                                                                       name="Attachments[5].files"
                                                                       class="requiredDocFile" value=""
                                                                       accept="application/pdf,application/vnd.ms-excel,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document">
                                                            </span>
                                                        </span>
                                                        <input type="text" class="form-control" readonly="readonly"
                                                               id="fileName_5" placeholder="Browse your file" value="">

                                                        <input type="hidden" name="DcDtlSupportingDo[5].docNameOld"
                                                               value="" id="DcDtlSupportingDoc[5].docNameOld">
                                                        <input type="hidden" name="DcDtlSupportingDo[5].docSizeOld"
                                                               value="" id="DcDtlSupportingDoc[5].docSizeOld">
                                                    </div>
                                                </td>
                                            </tr>
                                        </g:if>
                                        <g:if test="${docType.id.equals(7)}">

                                            <tr>
                                                <td>

                                                    <input type="hidden" name="DcDtlSupportingDo[6]._requiredDocId">
                                                    <input type="checkbox" name="Attachments[6].isActive"
                                                           id="requiredDocId_6">
                                                    <input type="hidden" name="Attachments[6].isActive" value="false"/>
                                                    <input type="hidden" name="Attachments[6].docId" value="6"
                                                           id="DcDtlSupportingDoc[6].typeId">
                                                    <input type="hidden" name="DcDtlSupportingDo[6].id" value=""
                                                           id="DcDtlSupportingDoc[6].id">
                                                </td>

                                                <td>

                                                    <label class="optional">${docType.docName}</label>

                                                    <%--<label class="optional">${documents.docName[6]}</label>--%>
                                                </td>


                                                <td>

                                                </td>

                                                <td>
                                                    <div class="input-group ">
                                                        <span class="input-group-btn">
                                                            <span class="btn btn-primary btn-file">
                                                                Browse …
                                                                <input type="file" id="docName_6"
                                                                       name="Attachments[6].files"
                                                                       class="requiredDocFile" value=""
                                                                       accept="application/pdf,application/vnd.ms-excel,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document">
                                                            </span>
                                                        </span>
                                                        <input type="text" class="form-control" readonly="readonly"
                                                               id="fileName_6" placeholder="Browse your file" value="">

                                                        <input type="hidden" name="DcDtlSupportingDo[6].docNameOld"
                                                               value="" id="DcDtlSupportingDoc[6].docNameOld">
                                                        <input type="hidden" name="DcDtlSupportingDo[6].docSizeOld"
                                                               value="" id="DcDtlSupportingDoc[6].docSizeOld">
                                                    </div>
                                                </td>
                                            </tr>
                                        </g:if>--}%
                                    </g:each>

                                    </tbody></table>
                            </div>
                        </div>
                    </fieldset>

                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" name="submit" class="btn btn-primary" id="submit"><i
                            class="fa fa-save"></i>Prepare Application
                    </button>
                    &nbsp;
                    <button type="submit" name="save" class="btn btn-info" id="update"><i
                            class="fa fa-save"></i>Save Application as Draft
                    </button>

                    <button type="reset" class="btn btn-warning pull-right"><i
                            class="fa fa-hand-paper-o"></i>Reset the Form
                    </button>
                </div><!-- /.box-footer -->
            </form>
        </div><!-- /.box box-primary -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
    function addMore(selectorClass) {
        var findPattern = /^(.*)(\d)+$/i;
        var selectorIndex = $("." + selectorClass).length;
        var cloneElement = "." + selectorClass + ":last";
        var removeBtn = '<div class="box-footer"><div class="pull-right"><a class="btn btn-block btn-danger btn-xs" onclick="removeDetail(\'' + selectorClass + '\', this);"><i class="fa fa-minus"></i>Remove</a></div></div>';

        $(cloneElement).clone().insertAfter(cloneElement).show()
                .find("*")
                .each(function () {
                    var id = this.id || "";
                    var match = id.match(findPattern) || [];
                    if (match.length == 3) {
                        var str = match[1].split("0");
                        this.id = str[0] + (selectorIndex)
                        this.name = this.name.replace(/\d+/, selectorIndex);//str[0] + (selectorIndex);
                    }
                });

        if ($(cloneElement + " > .box-footer").length == 0) {
            $(cloneElement + " > .box-body").after(removeBtn);
        }

        $('.datePickForSearch').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true
        });
        //        $('.datePickForSearch').datepicker('update');
        $(cloneElement).find('input').val('');
        $(cloneElement).find('textArea').val('');
        $(cloneElement).find('select').val('');
        selectorIndex++;
        return true;
    }
    function removeDetail(removeElementClass, _this) {
        var findPattern = /^(.*)(\d)+$/i;
        var removeElement = "." + removeElementClass;
        $(_this).closest(removeElement).remove();
        $(removeElement).each(function (i, obj) {
            $(obj).find("*")
                    .each(function (j, elem) {
                        var id = this.id || "";
                        var match = id.match(findPattern) || [];
                        if (match.length == 3) {
                            var str = match[1].split("0");
                            this.id = str[0] + (i)
                            this.name = this.name.replace(/\d+/, i);
                        }
                    });
        });
        return true;
    }
</script>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 3.2.1
    </div>

</footer>


<!--Date picker link-->
<%--<script type="text/javascript" src="<c:url value="/folder/mycssresource/datepicker/js/bootstrap-datepicker.min.js"/>"></script>--%>
<!-- Vendor JS-->
<asset:javascript src="/folder/userResources/vendor/datepicker/moment.min.js"></asset:javascript>
<asset:javascript src="/folder/userResources/vendor/datepicker/daterangepicker.js"></asset:javascript>
<!-- Main JS-->
<asset:javascript src="/folder/userResources/js/global.js"></asset:javascript>

<%--
<script type="text/javascript">
    var DATE_FORMAT = "dd/mm/yyyy";
    var DATE_FORMAT_JQ = "dd/mm/yy";
    var DOB_YEAR_RANGE = "-100:+0";
    var contextPath = "/Export";
    jQuery(document).ready(function () {
        contextPath = "/Export";
        $('.cb').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            increaseArea: '70%' // optional
        });
        $('.rb').iCheck({
            radioClass: 'iradio_square-blue',
            increaseArea: '70%' // optional
        });
    });
    //==========================================================
    // file name for user edit profile
    // $(".files").change(function () {
    $(document).on("change", ".files", function () {
        var value = this.value;
        if (value) {
            $(".docNameProxy").val(value);
        }
    });
    //***********************************************************
    //============================================================
    // Doc name for commercial module
    $(".requiredDocFile").change(function () {
        var id = this.id;
        var value = this.value;
        var idSegments = id.split("_");
        var rowId = idSegments[1];
        if (value) {
            $("#requiredDocId_" + rowId).prop("checked", true);
            $("#fileName_" + rowId).val(value);
        }
    });
    //************************************************************
    //=============================================================
    //Date picker for commercial module
    $('.datePickForSearch').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        todayHighlight: true

    });
    //******************************************************
    //=======================================================
    // collaper action for ep ip scp process & view page
    $(".collapse-btn").click(function () {
        var _this = this;
        $(_this).closest("legend").next(".collapse-block").slideToggle("slow", function () {
            $(_this).find("i").toggleClass("fa fa-minus fa fa-plus");
        });
    });
    //**************************************************
    //==================================================
    //**********************************************//

</script>
--%>

<script type="text/javascript">
    var DATE_FORMAT = "dd/mm/yyyy";
    var DATE_FORMAT_JQ = "dd/mm/yy";
    var DOB_YEAR_RANGE = "-100:+0";
    var contextPath = "/export";
    jQuery(document).ready(function () {
        contextPath = "/export";
        $('.cb').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            increaseArea: '70%' // optional
        });
        $('.rb').iCheck({
            radioClass: 'iradio_square-blue',
            increaseArea: '70%' // optional
        });
    });
    //==========================================================
    // file name for user edit profile
    // $(".files").change(function () {
    $(document).on("change", ".files", function () {
        var value = this.value;
        if (value) {
            $(".docNameProxy").val(value);
        }
    });
    //***********************************************************
    //============================================================
    // Doc name for commercial module
    $(".requiredDocFile").change(function () {
        var id = this.id;
        var value = this.value;
        var idSegments = id.split("_");
        var rowId = idSegments[1];
        if (value) {
            $("#requiredDocId_" + rowId).prop("checked", true);
            $("#fileName_" + rowId).val(value);
        }
    });
    //************************************************************
    //=============================================================
    //Date picker for commercial module
    $('.datePickForSearch').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        todayHighlight: true

    });
    //******************************************************
    //=======================================================
    // collaper action for ep ip scp process & view page
    $(".collapse-btn").click(function () {
        var _this = this;
        $(_this).closest("legend").next(".collapse-block").slideToggle("slow", function () {
            $(_this).find("i").toggleClass("fa fa-minus fa fa-plus");
        });
    });
    //**************************************************
    //==================================================
    //form action for commercial module
    $("button#submit").click(function () {
        $("#submitType").val("submit");
        $("form#checkForm").submit();
    });
    $("button#update").click(function () {
        $("#submitType").val("update");
        $("form#checkForm").submit();
    });
    $("button.sendBack").click(function () {
        $('#sendBackReason').prop('disabled', false);
        var sendBackReason = $('#sendBackReason').val();
        if (sendBackReason) {
            $("#submitType").val("sendBack");
            $("#checkForm").submit();
        }
    });
    $("#sendBackReason").focusout(function () {
        $('#sendBackReason').prop('disabled', true);
    });

    //**********************************************//
</script>

</body>
</html>
