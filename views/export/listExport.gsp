<%-- 
    Document   : Exportlist
    Created on : Jan 7, 2019, 9:37:54 PM
    Author     : Solimullah&Amena
--%>

<%@ page import="com.me.grailsmvc.ExportController" contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>

<html>
<head>
    <script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script>
    <script async="" src="https://www.google-analytics.com/analytics.js"></script>
    <script async="" src="https://www.googletagmanager.com/gtm.js?id=GTM-MFSJQFR"></script>

    <script>
        function deleteProduct(exportId) {
            if (confirm('Do you really want to delete this export?')) {
                var url = '/Export/export/deleteExport/'+ exportId;
                window.location.href = url;
            }
        }
    </script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    <!--style sheet link-->
    <asset:link rel="stylesheet" type="text/css" href="/folder/mycssresource/mycss.css"/>
    <asset:link rel="stylesheet" type="text/css" href="/folder/mycssresource/export/css/AdminLTE.min.css"/>
    <asset:link rel="stylesheet" type="text/css" href="/folder/mycssresource/export/css/bootstrap-datepicker.min.css"/>
    <asset:link rel="stylesheet" type="text/css" href="/folder/mycssresource/export/css/common.css"/>
    <asset:link rel="stylesheet" type="text/css" href="/folder/mycssresource/export/css/style.css"/>
    <asset:link rel="stylesheet" type="text/css" href="/folder/mycssresource/export/css/dataTables.bootstrap.css"/>

    <!--Date picker link-->
    <!-- Vendor CSS-->
    <%--<link rel="stylesheet" type="text/css" href="<c:url value="/folder/userResources/vendor/datepicker/daterangepicker.css"/>" media="all">--%>

    <!--for <i class="fa fa-reorder"></i> teg link-->
    <%--
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    --%>
    <!-- Or this Icons font CSS-->
    <asset:link rel="stylesheet" type="text/css"
                href="/folder/userResources/vendor/font-awesome-4.7/css/font-awesome.min.css" media="all"/>

</head>

<body class="skin-blue sidebar-mini sidebar-collapse pace-done">
<div class="pace  pace-inactive">
    <div class="pace-progress" data-progress-text="100%" data-progress="99"
         style="transform: translate3d(100%, 0px, 0px);">
        <div class="pace-progress-inner"></div>
    </div>

    <div class="pace-activity"></div>
</div>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper custom_mergin" style="min-height: 813px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><a href="/Export/">
            <img src="/Export/assets/bepzaLogo.png" height="50px" width="60px">
        </a>Export Permit Applications</h1>
        <ul class="top-links">
            <li>
                <a href="/Export/export/exportForm" class="btn btn-block btn-primary btn-xs">
                    <i class="fa fa-plus"></i> Add New Application
                </a>
            </li>
        </ul>
    </section><!-- /.content-header -->

    <section class="flash-message">
        <!-- flash message -->

        <!-- flash message end -->
    </section><!-- /.box-header (Flash Message) -->

    <section class="content"><!-- Main content -->
    <!-- Serch box -->
        <div class="box box-info">
            <form action="/bepza/dcMstDyeingCertificate/list" method="get">
                <fieldset>
                    <div class="box-header with-border">
                        <legend><h3 class="box-title">Search</h3></legend>

                        <div class="box-body">
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="form-group">
                                    <label>DC Permit No./Tracking No.</label>
                                    <input type="text" name="dcPermitNo" id="dcPermitNo" value="" class="form-control">
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="form-group">
                                    <label>Zone</label>
                                    <select id="dcZone" name="dcZoneId" class="form-control">
                                        <option value="">Select One</option>
                                        <option value="7">Cumilla Export Processing Zone</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="form-group">
                                    <label>Enterprise</label>
                                    <select id="enterprise" name="enterpriseId" class="form-control">
                                        <option value="">Select One</option>
                                        <option value="1768">Enzychim Iberica (BD) Limited</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="form-group">
                                    <label>Current Status</label>
                                    <select id="currentStatus" name="currentStatus" class="form-control">
                                        <option value="">Select One</option>
                                        <option value="37226">Checked and verified for meeting</option>
                                        <option value="37227">Update meeting decision</option>
                                        <option value="37228">Verify and update</option>
                                        <option value="37229">Placed in meeting</option>
                                        <option value="37243">Scrutiny</option>
                                        <option value="47790">Deny for cancel</option>
                                        <option value="2831">Draft</option>
                                        <option value="2832">Re Submitted</option>
                                        <option value="3000">Issued</option>
                                        <option value="2715">Prepared</option>
                                        <option value="27160">Submitted</option>
                                        <option value="2716">Checked &amp; Submitted</option>
                                        <option value="2717">Checked and Verified</option>
                                        <option value="2718">Forwarded</option>
                                        <option value="2721">Delegate</option>
                                        <option value="2719">Permitted by BEPZA</option>
                                        <option value="3099">Regret</option>
                                        <option value="2722">Import Delegate</option>
                                        <option value="2723">SCP Delegate</option>
                                        <option value="2724">Gate Pass Issued</option>
                                        <option value="2725">Gate Pass Rejected</option>
                                        <option value="2726">Application Extend</option>
                                        <option value="2727">Permit for Cancel</option>
                                        <option value="2728">Customs Clearance Issued</option>
                                        <option value="2729">Partial Gate Pass</option>
                                        <option value="2730">Partial Shipment (Close)</option>
                                        <option value="2731">Application Cancel</option>
                                        <option value="2732">Checked and Verified for Cancel</option>
                                        <option value="4320">Applied for Extension</option>
                                        <option value="27201">Revised</option>
                                        <option value="37224">Sent Back</option>
                                        <option value="4321">Applied for Cancel</option>
                                        <option value="5263">Forwarded for Engg. Verification</option>
                                        <option value="37314">Applied for Amendment</option>
                                        <option value="37225">Forwarded to Permit Officer</option>
                                        <option value="37214">Recommendation</option>
                                        <option value="37215">Mark to Process</option>
                                        <option value="37249">Delegate</option>
                                        <option value="37306">Scrutiny Found Ok</option>
                                        <option value="37307">Further Scrutiny</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="form-group">
                                    <label>Export Permit No.</label>
                                    <input type="text" name="exportPermitNo" id="exportPermitNo" value=""
                                           class="form-control">
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="form-group">
                                    <label>Commercial Invoice No.</label>
                                    <input type="text" name="invoiceNo" id="invoiceNo" value="" class="form-control">
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="form-group">
                                    <label>Submitted (From Date)</label>
                                    <input type="text" name="fromDate" id="fromDate" value=""
                                           class="form-control js-datepicker" placeholder="dd/mm/yyyy">
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="form-group">
                                    <label>Submitted (To Date)</label>
                                    <input type="text" name="toDate" id="toDate" value=""
                                           class="form-control js-datepicker" placeholder="dd/mm/yyyy">
                                </div>
                            </div>
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-search"></i>Search
                            </button>
                        </div><!-- /.box-footer-->

                    </div>
                </fieldset>
            </form>
        </div><!-- /.box -->

        <div class="box box-primary">
            <div class="box-body table-responsive no-padding">

            <table class="table table-bordered dt-responsive table-striped table-hover table-condensed display-3">
                <thead style="background-color: lightblue">
                <tr>
                    <th class="center">ID No.</th>
                    <th class="center">Tracking No.</th>
                    <th class="center">Zone Name</th>
                    <th class="center">Enterprise Name</th>
                    <th class="center">Invoice No.</th>
                    <th class="center">Current Status</th>
                    <th class="center">Update Date</th>
                    <th class="center">Action</th>
                </tr>
                </thead>
                <g:if test="${exports}"><!--means if exports exists (!exports means not exists)-->
                    <tbody>

                    <g:each in="${exports}" var="export">
                        <tr>
                            <td class="center">${export.id}</td>
                            <td class="center">${export.exPermitNo}</td>
                            <td class="center">${export.zoneName}</td>

                            <td class="left">${export.entprzName}</td>
                            <td class="left">${export.invoiceNo}</td>
                            <td class="left">Saved as Draft</td>
                            <td class="center">${export.exPermitDate}</td>
                            <td class="center">
                                <div class="list-action">

                                    <a href="/Export/export/viewExport/${export.id}"
                                       class="btn btn-primary btn-xs td-none" style="text-decoration: none" title="View"
                                       alt="View">
                                        <i class="fa fa-file-text-o"></i>
                                        View
                                    </a>
                                    <a href="${request.contextPath}/export/updateExport/${export.id}"
                                       class="btn btn-primary btn-xs td-none" style="text-decoration: none" title="View"
                                       alt="View">
                                        <i class="fa fa-edit"></i>
                                        Edit
                                    </a>
                                    <a href="#" onclick="javascript:deleteProduct(${export.id})"
                                       class="btn btn-primary btn-xs td-none" style="text-decoration: none" title="View"
                                       alt="View">
                                        <i class="fa fa-trash-o"></i>
                                        Delete
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </g:each>
                    </tbody>
                    </table>
                </g:if>
            <!-- For Pagination...-->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="pagination pull-right" id="pagination">
                        <g:paginate controller="export" action="listExport" total="${exportCount}" max="5"
                                    maxsteps="3"/> <!--here 1. max="" confirms number of records in a page; 2. maxsteps="" confirms steps of pagination number-->
                    </div>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
    $(document).ready(function () {
        $("#exportZone").change(function () {
            var zoneId = $(this).val();
            $.ajax({
                url: "/bepza/commonAjax/getEnterpriseByZoneId",
                data: {zoneId: zoneId},
                success: function (data, textStatus, jqXHR) {
                    console.log(data);
                    $("#enterprise").html(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                }
            });
        });
    });
</script>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 3.2.1
    </div>

</footer>
<!--        <link rel="stylesheet" type="text/css" href="/bepza/resources/plugins/iCheck/square/blue.css">
        <link rel="stylesheet" type="text/css" href="/bepza/resources/plugins/pace/pace.min.css">
        <link type="text/css" rel="stylesheet" href="/bepza/static/css/bootstrap-datepicker.min.css">
        <link type="text/css" rel="stylesheet" href="/bepza/resources/plugins/datatables/dataTables.bootstrap.css">
        <link type="text/css" rel="stylesheet" href="/bepza/resources/plugins/ionicons/css/ionicons.min.css">
        <link type="text/css" rel="stylesheet" href="/bepza/resources/plugins/jquery-confirm/jquery-confirm.min.css">
        <link type="text/css" rel="stylesheet" href="/bepza/resources/plugins/animate.css-3.5.1/animate.css">
        <link type="text/css" rel="stylesheet" href="/bepza/static/css/com/bepza/common/common.css">

        <script type="text/javascript" src="/bepza/resources/plugins/iCheck/icheck.min.js"></script>
        <script type="text/javascript" src="/bepza/resources/plugins/pace/pace.min.js"></script>
        <script type="text/javascript" src="/bepza/static/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="/bepza/resources/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="/bepza/resources/plugins/datatables/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="/bepza/resources/plugins/reflection/reflection.js"></script>
        <script type="text/javascript" src="/bepza/resources/plugins/reflection/custom-data-table.js"></script>
        <script type="text/javascript" src="/bepza/test/js/summernote/summernote.min.js"></script>
        <script type="text/javascript" src="/bepza/resources/plugins/jquery-confirm/jquery-confirm.min.js"></script>
        <script type="text/javascript" src="/bepza/static/js/com/bepza/common/custom-confirm.js"></script>
        <script type="text/javascript" src="/bepza/static/js/com/bepza/common/common.js"></script>
        -->


<%--<script type="text/javascript" src="<c:url value="/folder/mycssresource/export/js/bootstrap-datepicker.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/folder/mycssresource/export/js/common.js"/>"></script>
<script type="text/javascript" src="<c:url value="/folder/mycssresource/export/js/custom-confirm.js"/>"></script>
<script type="text/javascript" src="<c:url value="/folder/mycssresource/export/js/app.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/folder/mycssresource/export/js/analytics.js"/>"></script>

<script type="text/javascript" src="<c:url value="/folder/mycssresource/datepicker/js/bootstrap-datepicker.min.js"/>"></script>--%>
<asset:javascript src="/folder/mycssresource/export/js/jquery.dataTables.min.js"></asset:javascript>
<asset:javascript src="/folder/mycssresource/export/js/dataTables.bootstrap.min.js"></asset:javascript>
<asset:javascript src="/folder/mycssresource/export/js/custom-data-table.js"></asset:javascript>
<!-- Vendor JS-->
<asset:javascript src="/folder/userResources/vendor/datepicker/moment.min.js"></asset:javascript>
<asset:javascript src="/folder/userResources/vendor/datepicker/daterangepicker.js"></asset:javascript>
<!-- Main JS-->
<asset:javascript src="/folder/userResources/js/global.js"></asset:javascript>
<script type="text/javascript">
    var DATE_FORMAT = "dd/mm/yyyy";
    var DATE_FORMAT_JQ = "dd/mm/yy";
    var DOB_YEAR_RANGE = "-100:+0";
    var contextPath = "/bepza";
    jQuery(document).ready(function () {
        contextPath = "/bepza";
        $('.cb').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            increaseArea: '70%' // optional
        });
        $('.rb').iCheck({
            radioClass: 'iradio_square-blue',
            increaseArea: '70%' // optional
        });
    });
    //==========================================================
    // file name for user edit profile
    // $(".files").change(function () {
    $(document).on("change", ".files", function () {
        var value = this.value;
        if (value) {
            $(".docNameProxy").val(value);
        }
    });
    //***********************************************************
    //============================================================
    // Doc name for commercial module
    $(".requiredDocFile").change(function () {
        var id = this.id;
        var value = this.value;
        var idSegments = id.split("_");
        var rowId = idSegments[1];
        if (value) {
            $("#requiredDocId_" + rowId).prop("checked", true);
            $("#fileName_" + rowId).val(value);
        }
    });
    //************************************************************
    //=============================================================
    //Date picker for commercial module
    $('.datePickForSearch').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        todayHighlight: true

    });
    //******************************************************
    //=======================================================
    // collaper action for ep ip scp process & view page
    $(".collapse-btn").click(function () {
        var _this = this;
        $(_this).closest("legend").next(".collapse-block").slideToggle("slow", function () {
            $(_this).find("i").toggleClass("fa fa-minus fa fa-plus");
        });
    });
    //**************************************************
    //==================================================
    //form action for commercial module
    $("button#submit").click(function () {
        $("#submitType").val("submit");
        $("form#checkForm").submit();
    });
    $("button#update").click(function () {
        $("#submitType").val("update");
        $("form#checkForm").submit();
    });
    $("button.sendBack").click(function () {
        $('#sendBackReason').prop('disabled', false);
        var sendBackReason = $('#sendBackReason').val();
        if (sendBackReason) {
            $("#submitType").val("sendBack");
            $("#checkForm").submit();
        }
    });
    $("#sendBackReason").focusout(function () {
        $('#sendBackReason').prop('disabled', true);
    });

    //**********************************************//

</script>

</body>

</html>
