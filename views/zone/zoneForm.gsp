<%--
  Created by IntelliJ IDEA.
  User: Solimullah&Amena
  Date: 12-Oct-19
  Time: 12:19 PM
--%>

<%@ page import="com.me.grailsmvc.Zone; com.me.grailsmvc.Enterprise" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script>
    <script async="" src="https://www.google-analytics.com/analytics.js"></script>
    <script async="" src="https://www.googletagmanager.com/gtm.js?id=GTM-MFSJQFR"></script>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <g:javascript library="/Expoet/assets/folder/userResources/vendor/jquery/jquery.min.js"/>
    <g:javascript library="/Export/assets/folder/userResources/vendor/jquery/jquery.js"/>
    <!--style sheet link-->
    <asset:link rel="stylesheet" type="text/css" href="/folder/mycssresource/mycss.css"/>
    <asset:link rel="stylesheet" type="text/css" href="/folder/mycssresource/export/css/AdminLTE.min.css"/>
    <asset:link rel="stylesheet" type="text/css" href="/folder/mycssresource/export/css/bootstrap-datepicker.min.css"/>
    <asset:link rel="stylesheet" type="text/css" href="/folder/mycssresource/export/css/common.css"/>
    <asset:link rel="stylesheet" type="text/css" href="/folder/mycssresource/export/css/style.css"/>
    <asset:link rel="stylesheet" type="text/css" href="/folder/mycssresource/export/css/dataTables.bootstrap.css"/>

    <!--Date picker link-->
    <!-- Vendor CSS-->
    <%--<link rel="stylesheet" type="text/css" href="<c:url value="/folder/userResources/vendor/datepicker/daterangepicker.css"/>" media="all">--%>

    <!--for <i class="fa fa-reorder"></i> teg link-->
    <%--
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    --%>
    <!-- Or this Icons font CSS-->
    <asset:link rel="stylesheet" type="text/css"
                href="/folder/userResources/vendor/font-awesome-4.7/css/font-awesome.min.css" media="all"/>

    <script>
        function deleteProduct() {
            if (confirm('Do you really want to delete this country?')) {
                var url = '/Export/country/deleteCountry';
                window.location.href = url;
            }
        }
    </script>
</head>

<body class="skin-blue sidebar-mini sidebar-collapse pace-done">
<div class="pace  pace-inactive">
    <div class="pace-progress" data-progress-text="100%" data-progress="99"
         style="transform: translate3d(100%, 0px, 0px);">
        <div class="pace-progress-inner"></div>
    </div>

    <div class="pace-activity"></div>
</div>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper custom_mergin" style="min-height: 813px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h1><a href="/Export/">
            <img src="/Export/assets/bepzaLogo.png" height="50px" width="60px">
        </a>
            Zone and Enterprise</h1>
        <ul class="top-links">
            <li>
                <a href="/Export/country/countryForm" class="btn btn-block btn-primary btn-xs">
                    <i class="fa fa-plus"></i> Add New Application
                </a>
            </li>
        </ul>
    </section><!-- /.content-header -->

    <section class="flash-message">
    <!-- flash message -->
        <g:if test="${flash.message}">
            <section class="flash-message">
                <!-- flash message -->
                <div class="bs-example">
                    <div class="alert alert-success small fade in">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong style="font-size: 15px">${flash.message}</strong>
                    </div>
                </div>
                <!-- flash message end -->
            </section><!-- /.box-header (Flash Message) -->
        </g:if>

    <!-- For Error message-->
        <g:if test="${flash.error}">
            <section class="flash-message">
                <!-- flash message -->
                <div class="bs-example">
                    <div class="alert alert-error small fade in">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong style="font-size: 15px">${flash.error}</strong>
                    </div>
                </div>
                <!-- flash message end -->
            </section><!-- /.box-header (Flash Message) -->
        </g:if>
    <!-- flash message end -->
    </section><!-- /.box-header (Flash Message) -->

    <div class="content"><!-- Main content -->
        <section class="content"><!-- Main content -->
        <!-- Serch box -->
            <div class="box box-info">
                <form action="/Export/zone/saveEnterprise" method="post">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add New Enterprise (Zone-wise)</h3>

                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""
                                    data-original-title="Collapse"><i class="fa fa-minus"></i></button>

                        </div>
                    </div>

                    <div class="box-body">
                        <div class="row">

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>Zone Test</label>
                                    <g:select id="zoneId" name="zoneId" from="${Zone.list()}"
                                              noSelection="['': 'Select One']"
                                              optionKey="id"
                                              optionValue="zoneName"
                                              class="form-control input-sm "/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label>Name of Firm/Enterprise</label>
                                    <input type="text" name="enterpriseName" id="entrprsName" value=""
                                           class="form-control">
                                </div><!-- /.form-group -->
                            </div><!-- /.col -->

                        </div>

                        <div class="box-footer">
                            &nbsp;
                            <button type="submit" name="save" class="btn btn-info" id="update"><i
                                    class="fa fa-save"></i>Save Enterprise
                            </button>

                            <button type="reset" class="btn btn-warning pull-right"><i
                                    class="fa fa-hand-paper-o"></i>Reset the Form
                            </button>
                        </div>

                    </div>
                </form>
            </div>
        </section>
    </div>

    <div class="content"><!-- Main content -->
        <section class="content"><!-- Main content -->

            <div class="box box-body box-success">
                <h3 style="color: #d87a68">AJAX Test :</h3>

                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label>Zone Test</label>
                        <g:select id="exportZone" name="ExportPermit.exportZone.id" from="${Zone.list()}"
                                  optionKey="id"
                                  noSelection="['': 'Select One']" optionValue="zoneName"
                                  class="form-control input-sm "/>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label>Enterprise Test</label>
                        <g:select id="enterprise" name="ExportPermit.enterprise.id"
                                  from=""
                                  optionKey="id" noSelection="['': 'Select One']"
                                  optionValue=""
                                  class="form-control select2"/>
                    </div>
                </div>
            </div>

        </section>
    </div>
</div>


<script>
    $("#exportZone").change(function () {
        var zId = $(this).val();
        $.ajax({
            url: "${request.contextPath}/zone/getEnterpriseByZoneId",
            data: "zoneId=" + zId, // kye value pare...
            success: function (data, textStatus, jqXHR) {
                console.log(data);
                $("#enterprise").html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });
    });
</script>

</body>
</html>