<!DOCTYPE html>
<html>
<head>
    <%--<meta name="layout" content="main"/>--%>
    <title>Welcome to Grails</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
<div class="container" style="color: #001f3f">
    <div class="jumbotron">
        <table>
            <tr>
                <td>
                    <a href="/Export/">
                        <img src="/Export/assets/bepzaLogo.png">
                    </a>
                </td>
                <td>
                    <h2 align="center">&nbsp;&nbsp; Export Import System</h2>
                </td>
            </tr>
        </table>
    </div>
    <a href="/Export/export/exportForm.gsp">1. Export Application Form</a>
    <br/>
    <a href="${request.contextPath}/export/listExport?offset=0&max=5">2. Export List</a>
    <br/>
    <a href="${request.contextPath}/country/countryForm.gsp">3. Add Country</a>
    <br/>
    <a href="${request.contextPath}/country/listCountry?offset=0&max=5">4. Country List</a>
    <br/>
    <a href="${request.contextPath}/zone/zoneForm.gsp">5. Zone</a>
</div>
</body>
</html>
